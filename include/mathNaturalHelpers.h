#pragma once

#include <vector>
#include <set>

//! These routines are all very inefficient and should not be used with anything serious.
namespace math_natural_helpers
{
	template<typename T>
	void			getDigitsForBase(T number
						, short base
						, std::vector<short>& result);
	template<typename T>
	T				makeNumberFromDigits(std::vector<short>& digits
						, short base);

	long			getNaturalDivisorCount(long long int
						, std::set<long long>& primes);
	void			getNaturalDivisors(long long int number
						, std::set<long long>& primes
						, std::set<long long>& divisors);

	long long int	getNthTriangleNumber(long long int);

	long			getSumOfProperDivisors(long number
						, std::set<long long>& primes);

	void			getAbundantNumbers(const long start
								, const long last
								, std::vector<int>& result);

	void			power(const int base
						, const int power
						, std::vector<short>& outDecals);

	bool			divide(const long numerator
						, const long denumerator
						, long& outWhole
						, std::vector<int>& outDecimals
						, const int stopAtDecimal = 10);

	long			getNonOverlappingRecurrences(const std::string& where
						, const std::string& what);

	std::string		getLongestRecurrence(const std::string& value);

	std::string		getLongestTailRecurrence(const std::string& value);

	long long		getPentagonal(long long n);
	long long		getPentagonalIndex(long long n);

	long long		getHexagonal(long long n);
	long long		getHexagonalIndex(long long n);

	long long		getTriangle(long long n);
}

#include "mathNaturalHelpers.inl"