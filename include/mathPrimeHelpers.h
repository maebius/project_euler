#pragma once

#include <vector>
#include <set>
#include <utility>

//! These routines are all very inefficient and should not be used with anything serious.
namespace math_prime_helpers
{
	long getNthPrimeApproximationSieve(long);
	
	long getNthPrimeBruteForce(long);

	bool isPrimeRecursive(long);
	
	void getPrimeFactorsRecursive(const long&
		, std::vector<std::pair<long, int>>&);

	long getPrimeFactors(const long&
		, std::vector<std::pair<long, int>>&
		, std::set<long long>&);

	void getPrimesBelowEratosthenesSieve(long
		, std::set<long long>&);
	
	void getPrimesBelowEratosthenesSieveVector(long
		, std::vector<bool>&);
}