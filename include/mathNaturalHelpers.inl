
namespace math_natural_helpers
{
	template<typename T>
    void getDigitsForBase(T number, short base, std::vector<short>& result)
    {
        while (number > 0)
        {
            result.push_back(static_cast<short>(number % base));
            number /= base;
        }
    }

	template<typename T>
	T makeNumberFromDigits(std::vector<short>& digits, short base)
	{
		T result = 0;
		T m = 1;
		for (int i = digits.size() - 1; i != -1; i--)
		{
			result += digits[i] * m;
			m *= base;
		}
		return result;
	}
}