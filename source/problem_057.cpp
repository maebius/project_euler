#include <iostream>
#include <vector>
#include "SimpleTimer.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");
#include <boost/multiprecision/cpp_int.hpp>
#include "mathNaturalHelpers.h"

using namespace std;
using namespace math_natural_helpers;
using namespace boost::multiprecision;

namespace project_euler
{

/*
It is possible to show that the square root of two can be expressed as an 
infinite continued fraction.

√ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...

By expanding this for the first four iterations, we get:

1 + 1/2 = 3/2 = 1.5
1 + 1/(2 + 1/2) = 7/5 = 1.4
1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...

The next three expansions are 99/70, 239/169, and 577/408, but the eighth 
expansion, 1393/985, is the first example where the number of digits in the 
numerator exceeds the number of digits in the denominator.

In the first one-thousand expansions, how many fractions contain a numerator 
with more digits than denominator?

*/
// -> OK (153)

	template<typename T>
	void getNext(T& nominator, T& denominator)
	{
		T temp = denominator;
		denominator = nominator;
		nominator = 2 * denominator + temp;
	}

	template<typename T>
	void getThis(T& nominator, T& denominator)
	{
		T temp = denominator;
		denominator = nominator;
		nominator = denominator + temp; 
	}

	void problem_057()
	{
		time_helpers::SimpleTimer<> timer;

		const int start = 3;
		const int last = 1000;

		using Precision = cpp_int;

		Precision nominator = 5;
		Precision denominator = 2;

		int moreDigitsInNom = 0;

		for (int i = start; i <= last; i++)
		{
			getNext<Precision>(nominator, denominator);

			//cout << "temp: " << nominator << " / " << denominator << endl;

			Precision thisN = nominator;
			Precision thisD = denominator;	
			getThis<Precision>(thisN, thisD);

			vector<short> nomVec;
			vector<short> denVec;
			getDigitsForBase<Precision>(thisN, 10, nomVec);
			getDigitsForBase<Precision>(thisD, 10, denVec);

			if (nomVec.size() > denVec.size())
			{
				moreDigitsInNom++;
				//cout << "exp[" << i << "]: " << thisN << " / " << thisD << endl;
			}
		}

		cout << moreDigitsInNom << endl;

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}