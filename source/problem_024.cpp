#include <iostream>
#include <vector>
#include <string>

#include "mathCombinatorics.h"

using namespace std;

namespace project_euler
{

    void fillPermutations(string& current, const vector<int>& elements, vector<bool>& check, int iteration, vector<string>& result)
    {
        if (iteration == check.size())
        {
            result.push_back(current);
            return;
        }
        for (int i = 0; i < elements.size(); i++)
        {
            if (check[i])
            {
                continue;
            }
            int elem = elements[i];
            string s = current + to_string(elem);
            check[i] = true;
            fillPermutations(s, elements, check, iteration+1, result); 
            check[i] = false;
        }
    }

/*
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/
    //! -> OK (2783915460)
    void problem_024()
    {
        const int wantedOrder = 1000000;

        vector<string> permutations;

        vector<int> elements { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        vector<bool> check ( elements.size(), false );

        string s{""};
        fillPermutations(s, elements, check, 0, permutations);

        //cout << "permutations: " << permutations.size() << endl;

        sort(permutations.begin(), permutations.end());

        cout << permutations[wantedOrder - 1] << endl;

        //for (auto a : permutations) cout << a << endl;
        //cout << math_combinatorics::getFactorial<long>(10) << endl;
    }
}