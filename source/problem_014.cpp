#include <iostream>
#include <map>

using namespace std;

namespace project_euler
{
    long getNextCollatzSequenceNumber(long n)
    {
        if (n % 2 == 0)
        {
            return n / 2;
        }
        else
        {
            return 3 * n + 1;
        }
    }

    long processNumber(long number, map<long, long>& triedCache)
    {
        if (triedCache.find(number) != triedCache.end())
        {
            return triedCache[number];
        }
        long next = getNextCollatzSequenceNumber(number);
        long nextLength = processNumber(next, triedCache);
        triedCache[number] = nextLength + 1;

        //cout << number << " : " << triedCache[number] << endl; 
        
        return triedCache[number];
    }

    /*
    The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*/

    // -> OK (837799]
    void problem_014()
    {
        map<long, long> triedCache;
        triedCache[1] = 1;

        long longest = 0;
        long index = 0;

        const long start = 999999;
        const long last = 1;
        //const long start = 9;
        //const long last = 9;
        
        for (long i = start; i >= last; i--)
        {
            long length = processNumber(i, triedCache);
            if (length > longest)
            {
                longest = length;
                index = i;
            }
        }
        cout << "longest: " << index << " with seq length: " << longest << endl; 
    }
}