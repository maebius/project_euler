#include <iostream>
#include "SimpleTimer.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

#include "mathCombinatorics.h"

using namespace std;
using namespace math_combinatorics;

namespace project_euler
{
/*
There are exactly ten ways of selecting three from five, 12345:

123, 124, 125, 134, 135, 145, 234, 235, 245, and 345

In combinatorics, we use the notation, 5C3 = 10.

In general,

nCr =	
n! /
(r!(n−r)!)

where r ≤ n, n! = n×(n−1)×...×3×2×1, and 0! = 1.

It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.

How many, not necessarily distinct, values of  nCr, for 1 ≤ n ≤ 100, 
are greater than one-million?
*/

// OK -> 4075

	template<typename T>
	std::set<T> makeSet(const T& first, const T& last, const T& increment)
	{
		std::set<T> result;
		T current = first;
		while (current <= last)
		{
			result.insert(current);
			current = current + increment;
		}
		return result;
	}

	int brute_force(int first, int last, int limit)
	{
		int current = first;
		int biggerCount = 0;

		while (current <= last)
		{
			auto elements = makeSet<int>(1, current, 1);

			for (int i = 1; i <= elements.size(); i++)
			{
				auto per = getPermutations<int>(elements, i);
				if (per.size() > limit)
				{
					//cout << "elements: " << current << ", binSize: " << i << ", combinations: " << per.size() << endl;
					biggerCount++;
				}
			}
			current++;
		}

		/*for_each(per.begin(), per.end(), [](vector<int>& v) 
			{ 
				for_each(v.begin(), v.end(), [](int n) { cout << n << " "; });
				cout << endl;
			});*/

		return biggerCount;
	}

	using namespace boost::multiprecision;

	int counting_values_brute(int first, int last, int limit)
	{
		int biggerCount = 0;
		for (int n = first; n <= last; n++)
		{
			for (int c = 1; c <= n; c++)
			{
				auto b = getBinomialCoefficientT<cpp_int>(c, n);
				//cout << "(brute) n: " << n << ", c: " << c << ", b: " << b << endl;
				if (b > limit)
				{
					biggerCount++;
				}
			}
		}
		return biggerCount;
	}


	int counting_values(int first, int last, int limit)
	{
		int biggerCount = 0;

		for (int n = first; n <= last; n++)
		{
			// special case
			if (n == 1)
			{
				if (n > limit)
				{
					biggerCount++;
				}
				continue;
			}

			// biggest value lies in the middle
			int pivot = n / 2;
			
			auto atPivot = getBinomialCoefficientT<cpp_int>(pivot, n);

			/* cout	<< "n: " << n << ", pivot: " << pivot 
					<< ", value: " << atPivot << endl; */

			// we can skip altogether if not even this hits the limit
			if (atPivot < limit)
			{
				continue;
			}
			// increment at the pivot itself
			biggerCount++;
			// check the possible odd part
			if (n % 2 != 0 
				&& getBinomialCoefficientT<cpp_int>(pivot + 1, n) > limit)
			{
				biggerCount++;
			}

			// check the ones that are below/above, with binary search	
			int start = 1;
			int end = pivot;
			 
			while (true)
			{
				int p = (end + start) / 2;
				auto v = getBinomialCoefficientT<cpp_int>(p, n);
				
				/* cout	<< "searching smallest, current binSize: " 
						<< p << ", value: " << v << " [" 
						<< start << ", " << end << "]" << endl; */
				
				if (v > limit)
				{
					if (end == p)
					{
						break;
					}

					end = p;
				}
				else
				{
					if (start == p)
					{
						break;
					}
					start = p;
				}
			}
			
			// the range is symmetric, so we can multiply by two
			auto add = (pivot - end) * 2;
			//cout << "add: " << add << " (n: " << n << ")" << endl;
			biggerCount += add;
		}

		return biggerCount;
	}

	void problem_053()
	{
		//const int limit = 1000000;
		//const int first = 1;
		//const int last = 100;
		
		const int limit = 1000000;
		const int first = 1;
		const int last = 100;

		time_helpers::SimpleTimer<> timer;

		cout << "result: " << counting_values(first, last, limit) << endl;

		cout << "took [ms]: " << timer.getElapsed() << endl;   
		timer.restart();

		cout << "brute: " << counting_values_brute(first, last, limit) << endl;
		cout << "took [ms]: " << timer.getElapsed() << endl;   
		timer.restart();

/* 		cout << "2xbrute: " << brute_force(first, last, limit) << endl;
		cout << "took [ms]: " << timer.getElapsed() << endl;   
		timer.restart(); */
	}
}