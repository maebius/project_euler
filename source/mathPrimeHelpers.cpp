#include "mathPrimeHelpers.h"
#include <iostream>
#include <cmath>
#include <set>
#include <cassert>

using namespace std;

namespace math_prime_helpers
{
    long getNthPrimeBruteForce(long n)
    {
        // from here we could devise more efficient algorithm:
        // http://math.stackexchange.com/questions/507178/most-efficient-algorithm-for-nth-prime-deterministic-and-probabilistic?noredirect=1&lq=1

        long currentN = 1;
        long val = 3;

        while (currentN < n)
        {
            if (isPrimeRecursive(val))
            {
                currentN++;
            }
            val += 2;
        }

        return val -= 2;
    }

    long getNthPrimeApproximationSieve(long n)
    {
        if (n <= 5)
        {
            static const long smallAnswers [] { 2, 3, 5, 7, 11 };
            return smallAnswers[n - 1];
        }

        double ln = log(n);
        double lln = log(ln); 
        // this is from Prime Number Theorem, but the boundary can contain multiple primes, return the first one
        long lowBound = (long) (n * (ln + lln - 1));
        long highBound = ceil(n * (ln + lln));

        cout << "sieving in bounds [" << lowBound << ", " << highBound << "]" << endl;

        for (long c = lowBound; c < highBound; c++)
        {
            if (isPrimeRecursive(c))
            {
                return c;
            }
        }
        // should not get here
        return -1;
    }
 
    bool isPrimeRecursion(long current, const long max, const long number)
    {
        if (current >= max)
        {
            return true;
        }
        if (number % current == 0)
        {
            return false;
        }
        return isPrimeRecursion(current + 1, max, number);
    }

    //! Very poor method. If not Release/Optmized build, tail-recursion does not disappear and will choke 8GB with number ~340 000.
    bool isPrimeRecursive(long number)
    {
        long max = number / 2;
        return isPrimeRecursion(2, max, number);
    }

    //! Uses enormous amount of memory for large limits. ~ O(N)
    void getPrimesBelowEratosthenesSieve(long limit, std::set<long long>& result)
    {
        assert(limit > 3);

        const long printDelta = 10000;
        long printLimit = printDelta;

        vector<bool> cache(limit, true);
        for (int i = 4; i < limit; i+=2)
        {
            cache[i] = false;
        }
        // we know 2 is prime
        result.insert(2);

        long realLimit = ceil(sqrt(limit));
        long current = 3;
        while (current < realLimit)
        {
            if (current > printLimit)
            {
                cout << current << " / " << limit << endl;
                printLimit += printDelta;
            }

            if (cache[current])
            {
                result.insert(current);

                long nonPrime = current * current;
                while (nonPrime < limit)
                {
                    cache[nonPrime] = false;
                    nonPrime += 2*current;
                }
            }

            // we can skip every even number
            // ... and also all above sqrt(limit)
            // ... and the ones marked as multiples of primes
            current+=2;
            while (current < realLimit && !cache[current])
            {
                current+=2;
            }
        }
        // need to add the ones that might come after sqrt(limit)
        while (current < limit)
        {
            if (cache[current])
            {
                result.insert(current);
            }
            current+=2;
        }
    }

     //! Uses enormous amount of memory for large limits. ~ O(N)
    void getPrimesBelowEratosthenesSieveVector(long limit, std::vector<bool>& cache)
    {
        assert(limit > 3);

        /*
        const long printDelta = 10000;
        long printLimit = printDelta;
        */
        
        cache.resize(limit, true);
        for (int i = 4; i < limit; i+=2)
        {
            cache[i] = false;
        }
        cache[0] = false;
        cache[1] = false;

        long realLimit = ceil(sqrt(limit));
        long current = 3;
        while (current < realLimit)
        {
            /*
            if (current > printLimit)
            {
                cout << current << " / " << limit << endl;
                printLimit += printDelta;
            }*/

            if (cache[current])
            {
                long nonPrime = current * current;
                while (nonPrime < limit)
                {
                    cache[nonPrime] = false;
                    nonPrime += 2*current;
                }
            }

            // we can skip every even number
            // ... and also all above sqrt(limit)
            // ... and the ones marked as multiples of primes
            current+=2;
            while (current < realLimit && !cache[current])
            {
                current+=2;
            }
        }
    }

    void getPrimeFactorsRecursion(long number, long current, const long& max, std::vector<std::pair<long,int>>& result)
    {
        // if our current number is already 1, we've at maximum depth in recursion
        // - also if at max (half the original number), means we can't divide the number evenly anymore (would be less than 2)
        if (number == 1 || current > max)
            return;
        
        //cout << "[" << number << "] " << current << endl;
    
        // only consider Primes themselves, and if we can divide our current number with it
        if (isPrimeRecursive(current)
            && number % current == 0)
        {
            int power = 0;
            // get rid of possible multiples of this factor
            do 
            {
                power++;
                number /= current;
            }
            while (number % current == 0);

            //cout << "[" << number << "] " << current << "^" << power << endl;
        
            result.push_back(std::pair<long, int>(current, power));
        }
        // recurse
        getPrimeFactorsRecursion(number, current+1, max, result);
    }

    void getPrimeFactorsRecursive(const long& number, std::vector<std::pair<long, int>>& result)
    {
        // recursively take the factors:
        // - start by trying to divide by 2, then incrementing from that all the way to number/2
        // - only consider by dividing with numbers that are Primes themselves

        long max = number / 2;
        getPrimeFactorsRecursion(number, 2, max, result);
    }

    long getPrimeFactors(const long& number, std::vector<std::pair<long, int>>& result, std::set<long long>& primes)
    {
        long remainder = number;

        //cout << "primes: "; for (auto prime : primes) cout << prime << " "; cout << endl;

        for (auto prime : primes)
        {
            if (remainder % prime == 0)
            {
                int power = 0;
                // get rid of possible multiples of this factor
                do 
                {
                    power++;
                    remainder /= prime;
                }
                while (remainder % prime == 0);

                //cout << "[" << number << "] " << prime << "^" << power << " (remaining: " << remainder << ")" << endl;
            
                result.push_back(std::pair<long, int>(prime, power));

                if (remainder == 1)
                {
                    break;
                }
            }
        }
        
        // NOTE! If getting here, making routine very slow ... would be better to guess better initial primes
        if (remainder > *primes.rbegin())
        {
            const long limit = number * 10;

            //cout << "getPrimeFactors - number: " << number << ", remainder: " << remainder << ", prev biggest prime: " << *primes.rbegin() << ", new limit: " << limit << endl;

            primes.clear();
            result.clear();

            math_prime_helpers::getPrimesBelowEratosthenesSieve(limit, primes);
            remainder = getPrimeFactors(number, result, primes);
        }
        if (remainder != 1)
        {
            cout << "getPrimeFactors - error for: " << number << ", remainder = " << remainder << endl;
        }
        return remainder;
    }
}