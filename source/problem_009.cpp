#include <iostream>
#include <cmath>

using namespace std;

namespace project_euler
{
    bool is_integer(float number)
    {
        return std::floor(number) == number;
    }

    bool findPythagoreanTripletWithSum(long sum, int& outA, int& outB, int& outC)
    {
        // solving a + b + c = 1000 for c, putting that to Puthagorean theorem and
        // solving for b gives:
        // b = (10^6 - 2000 * a) / 2 * (1000 - a) 

        // we need to find a value for a that makes b to be a natural number 
        // and also a < b < c

        for (int a = 2; a < (sum - 2)/2; a++)
        {
            int nominator = sum*sum - 2 * sum * a;
            int denominator = 2 * (sum - a);

            // NOTE! could use is_integer(1.f * nominator / denominator) also
            if (nominator % denominator == 0)
            { 
                int b = nominator / denominator;
                int c = sum - a - b;
                if (b > a && c > b && c > a)
                {
                    outA = a;
                    outB = b;
                    outC = c;
                    return true;
                }
            }  
        } 
        return false;
    }

    //! A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
    // a^2 + b^2 = c^2
    // For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
    // There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    // Find the product abc.
    // -> OK (answer 31875000)
    void problem_009()
    {
        const long sum = 1000;
        int a, b, c;
        if (findPythagoreanTripletWithSum(sum, a, b, c))
        {
            cout    << "Result for sum " << sum 
                    << " where a = " << a << ", b = " << b << ", c = " << c
                    << ", a*b*c = " << (a*b*c) << endl;
        }
        else
        {
            cout << "No result for sum " << sum << endl;
        }
    }
}