#include <iostream>
#include <map>
#include <vector>
#include <set>
#include "mathCombinatorics.h"

using namespace std;

namespace project_euler
{
/*
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, 
the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, 
containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
*/
    //! -> OK (45228)
    struct ResultItem
    {
        long multiplier;
        long multiplicand;
        long product;

        bool operator<(const ResultItem& rhs) const
        {
            if (product == rhs.product)
            {
                return multiplier < rhs.multiplier;
            }

            return product < rhs.product;
        } 

        bool operator==(const ResultItem& rhs) const
        {
            return product == rhs.product;
        }
    };

    const vector<int> g_digits { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  
    long getNumber(const vector<int>& item, int start, int last)
    {
        long result = 0;
        long decal = 1;
        for (int i = last; i >= start; i--)
        {
            result += (item[i] * decal);
            decal *= 10;
        }

        cout << "getNumber(start=" << start << ", last=" << last << ") = " << result << endl;
        std::for_each(item.begin(), item.end(), [](const int v) { cout << v; }); cout << endl;

        return result;
    }

    void problem_032()
    {
        long l = math_combinatorics::getFactorial<long>(g_digits.size());

        auto combinations = math_combinatorics::getPandigitals<int>(g_digits);
        
        //cout << "should count: " << l << ", was: " << combinations.size() << endl;

        int maxLength = g_digits.size() / 2;

        vector<ResultItem> results;

        for (auto comb : combinations)
        {
            //std::for_each(comb.begin(), comb.end(), [](const int v) { cout << v; }); cout << endl;

            // fl = length of multiplier
            for (int fl = 1; fl <= maxLength; fl++)
            {
                long multiplier = getNumber(comb, 0, fl - 1);

                //cout << "fl=" << fl << ", multiplier=" << multiplier << ", last=" << maxLength << endl;

                // sl = length of multiplicand
                for (int sl = fl; sl <= maxLength; sl++)
                {
                    long multiplicand = getNumber(comb, fl, sl);
                    long product = multiplier * multiplicand; 
                    long shouldProduct = getNumber(comb, sl+1, g_digits.size() - 1);

                    //cout << multiplier << " * " << multiplicand << " ?= " << shouldProduct << endl;
                 
                    if (product == shouldProduct)
                    {
                        //cout << multiplier << " * " << multiplicand << " = " << shouldProduct << endl;
                        results.push_back(ResultItem { multiplier, multiplicand, product});
                    }
                }
            }
        }

        //cout << "[" << results.size() << "]" << endl;

        std::sort(results.begin(), results.end());
        results.erase(std::unique(results.begin(), results.end()), results.end());

        long totalSum = 0;
        for (auto r = results.begin(); r != results.end(); r++)
        {
            cout << r->multiplier << " * " << r->multiplicand << " = " << r->product << endl;
            totalSum += r->product;
            
        }
        cout << "[" << results.size() << "] sum: " << totalSum << endl;
    }
}