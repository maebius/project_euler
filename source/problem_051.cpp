#include <iostream>
#include "SimpleTimer.h"
#include "RandomHelpers.h"
#include "MathHelpers.h"
#include "mathPrimeHelpers.h"
#include "mathNaturalHelpers.h"
#include "mathCombinatorics.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

using namespace std;
using namespace math_natural_helpers;

namespace project_euler
{
/*
By replacing the 1st digit of the 2-digit number *3, it turns out that six of 
the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, 
this 5-digit number is the first example having seven primes among 
the ten generated numbers, yielding the family: 
56003, 56113, 56333, 56443, 56663, 56773, and 56993. 
Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number 
(not necessarily adjacent digits) with the same digit, is part of an eight prime value family.

=> OK (121313)

*/

    bool binarySearchBestFromSorted(
        long long target
        , std::vector<long long>& input 
        , size_t& index)
    {
        size_t first = 0;
        size_t last = input.size() - 1;
        index = 0;

        while (first != last)
        {
            //cout << first << " - " << last << endl;

			index = (last + first) / 2;
        
            if (input[index] == target)
            {
                return true;
            }
            else if (index == first)
            {
                break;
            }
            else if (input[index] > target)
            {
                last = index;
            }
            else
            {
                first = index;
            }
        }
        return false;
    }

    void testBinarySearch()
    {
        int minVal = 0;
        int maxVal = 10000;
        int count = 50;
        long long target = maxVal / 2;

        std::vector<long long> result;
        random_helpers::fill(minVal, maxVal, count, result);

        //result[count/2] = target;

        std::sort(result.begin(), result.end());

        for (auto p : result)
        {
            cout << p << " ";
        }
        
        size_t index;
        auto found = binarySearchBestFromSorted(target, result, index);

        cout << "found: " << found << ", best index: " << index << ", best value: " << result[index] << endl;

    }

	void makeCombinationsByReplacingRecursion(
		const std::vector<short>& input
		, std::vector<std::vector<short>>& results
		, std::vector<short> intermediate
		, int count
		, short to
		, int left
		, int currentIndex)
	{
		if (input.size() - currentIndex < left)
			return;

		if (left > 0)
		{
			std::vector<short> fill{intermediate};
			fill.push_back(to);

			makeCombinationsByReplacingRecursion(
				input
				, results
				, fill
				, count
				, to
				, left - 1
				, currentIndex + 1);

			std::vector<short> no_fill{intermediate};
			no_fill.push_back(input[currentIndex]);

			makeCombinationsByReplacingRecursion(
				input
				, results
				, no_fill
				, count
				, to
				, left
				, currentIndex + 1);
		}
		else
		{
			for (int i = currentIndex; i < input.size(); i++)
			{
				intermediate.push_back(input[i]);
			}
			results.push_back(intermediate);

			//for (auto e : intermediate) cout << e << " "; cout << endl; 
			//if (results.size() > 5) return; // DEBUG!!!
		}
	}

	std::vector<std::vector<short>> makeCombinationsByReplacing(
		const std::vector<short>& input, int count, short to)
	{
		std::vector<std::vector<short>> results;
		std::vector<short> buffer;
		
		makeCombinationsByReplacingRecursion(
			input
			, results
			, buffer
			, count
			, to
			, count
			, 0);

		return results;
	}


	vector<short> replaceWith(vector<short> input, short from, short to)
	{
		//cout << "replaceWith - from: " << from << ", to: " << to << endl;
		//for (auto i : input) cout << i; cout << endl;

		vector<short> result;

		std::for_each(input.begin(), input.end(), 
			[&result, &from,  &to](short v) 
			{ 
				result.push_back((v == from) ? to : v);
			});

		return result;
	}

	bool isPrime(long number, std::vector<long long>& primes)
	{
		size_t index;
		return binarySearchBestFromSorted(number, primes, index);
	}

	set<long long> getPrimesByReplacing(std::vector<short>& comb
		, std::vector<long long>& primeVec)
	{
		set<long long> resultPrimes;
		for (int n = 0; n <= 9; n++)
		{	
			if (n == 0 && comb[0] == -1)
				continue;

			//cout << "combination: ";
			//for (int i = 0; i < comb.size(); i++) cout << comb[i]; cout << endl;
			
			auto candidateVec = replaceWith(comb, -1, n);		
			long long candidate = makeNumberFromDigits<long long>(candidateVec, 10);

			if (isPrime(candidate, primeVec))
			{
				resultPrimes.insert(candidate);
				//cout << "was prime: " << candidate << " (replacing " << c << " digits with " << n << ", result.size=" << resultPrimes.size() << ")" << endl;
			}
		}
		return resultPrimes;
	} 

	long long getSmallestPrimeFromDigitReplacement(
		long long startNumber
		, long targetPrimeCount
		, std::vector<long long>& primeVec)
	{
		size_t index;
		binarySearchBestFromSorted(startNumber, primeVec, index);

		// we can require the candidate to be prime,
		// as however we do the masking-combinations, the intention is to
		// get the primes
		// -> we might get to the actual from "earlier non-prime number",
		// but we still do with the "later prime", as the answer needs to be
		// prime anyway

		for (long i = index + 1; i < primeVec.size(); i++) // this to optimize by checking only primes
		//for (long number = startNumber; number < primeVec[primeVec.size()-1]; number++) // brute force
		{
			auto number = primeVec[i];
			
			vector<short> result;
			math_natural_helpers::getDigitsForBase(number, 10, result);
			std::reverse(result.begin(), result.end());
			
			auto digitCount = result.size();

			for (int c = 1; c <= digitCount; c++)
			{
				auto combinations = makeCombinationsByReplacing(result, c, -1);

				//cout << "replacing " << c << " digits" << endl;
				
				for (auto comb : combinations)
				{
					set<long long> resultPrimes = 
						getPrimesByReplacing(comb, primeVec);

					if (resultPrimes.size() == targetPrimeCount)
					{
						cout << "FOUND RESULT (" << targetPrimeCount << " primes)" << endl;
						cout << "source number: " << number << endl;
						cout << "replacing elements: " << c << endl;
						cout << "pattern: ";
						for (int i = 0; i < comb.size(); i++) 
						{
							if (comb[i] < 0)
							{
								cout << "x";
							}
							else
							{
								cout << comb[i];
							}
						}
						cout << endl;
						cout << "generated numbers: " << endl;

						std::for_each(resultPrimes.begin(), resultPrimes.end()
							, [](const long long& rp) { cout << rp << endl; });
						
						return *resultPrimes.begin();
					}
				}
			}
		}
		return -1;
	}

    void problem_051()
    {
        time_helpers::SimpleTimer<> timer;

        long long limit = 1000000;
        std::set<long long> primes;

		math_prime_helpers::getPrimesBelowEratosthenesSieve(limit, primes);
		std::vector<long long> primeVec(primes.begin(), primes.end());

		long targetPrimeCount = 8;

		//long long startNumber = 56003;
		long long startNumber = 56994;

		auto result = getSmallestPrimeFromDigitReplacement(
			startNumber, targetPrimeCount, primeVec);

		cout << "RESULT: " << result << endl;

        long elapsedTimeMs = timer.getElapsed();
        cout << "took [ms]: " << elapsedTimeMs << endl;   
    }
}