#include <iostream>
#include "SimpleTimer.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

#include "mathNaturalHelpers.h"

using namespace std;
using namespace math_natural_helpers;

namespace project_euler
{
/*
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.
Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
*/

	// -> OK (142857)
	bool containsOnlySameDigits(const std::vector<long long>& elements)
	{
		//cout << "containsOnlySameDigits - input: ";
		//for_each(elements.begin(), elements.end(), [](long long e) { cout << e << " "; });
		//cout << endl;

		vector<short> firstDigits;
		getDigitsForBase(elements[0], 10, firstDigits);

		for (int i = 1; i < elements.size(); i++)
		{
			// trivial case, the numbers are same so digits are same
			if (elements[i] == elements[0])
				continue;
			
			vector<short> digits;
			getDigitsForBase(elements[i], 10, digits);

			// NOTE: this check comes too late regarding efficiency
			if (firstDigits.size() != digits.size())
			{
				return false;
			}

			for (int d = 0; d < firstDigits.size(); d++)
			{
				// find first occurence from this element
				auto r = find(digits.begin(), digits.end(), firstDigits[d]);
				if (r == digits.end())
				{
					//cout << "did not find " << firstDigits[d];
					//cout << " (" << d << ". from " << elements[0];
					//cout << ", tried to find from " << elements[i] << ")";
					//cout << endl;

					// did not find so the number is not a permutation of the 1st
					return false;
				}
				else
				{
					// found the number, taking it now away as handled
					digits.erase(r);
				}
			}
		}
		// all numbers in the vector were permutations of the 1st one
		return true;
	}

	template<typename T>
	vector<T> getConsecutiveMultiples(T source, T startCe, T endCe)
	{
		vector<T> results;
		T current = source * startCe;
		for (T i = startCe; i <= endCe; i++)
		{
			results.push_back(current);
			current += source;
		}
		return results;
	}

	long getSmallest(long start, long multiplyFactor)
	{
		for (long i = start; ; i++)
		{
			auto c = getConsecutiveMultiples<long long>(
				i
				, 1
				, multiplyFactor);
			
			if (containsOnlySameDigits(c))
			{
				return i;
			}
			//return -1; // DEBUG
		}
		return -1;
	}


	void problem_052()
	{
		time_helpers::SimpleTimer<> timer;

		auto result = getSmallest(1, 6);
		//auto result = getSmallest(1, 2);
		cout << "result: " << result << endl;

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}