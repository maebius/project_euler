#include <iostream>
#include <set>
#include "SimpleTimer.h"
#include "mathPrimeHelpers.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");
using namespace std;

const char* g_profilerName = "profiler";

namespace project_euler
{
/*
The prime 41, can be written as the sum of six consecutive primes:

41 = 2 + 3 + 5 + 7 + 11 + 13
This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?
*/

    //! -> OK (997651)

    //! Requires "a" to be in increasing order. Returns true if found, false if not (in which case outResultIndex will have
    //! the index of element that is the first below the value we are searching.
    bool binarySearchRecurse(const long long& value
        , const long start
        , const long end
        , const std::vector<long long>& a
        , long & outResultIndex)
    {
        //cout << "recurse, find: " << value << ", start: " << a[start] << " [" << start << "], end: " << a[end] << " [" << end << "]" << endl;
        
        outResultIndex = (start + end) / 2;
        const long long& pivot = a[outResultIndex];

        //cout << "pivot: " << pivot << " [" << outResultIndex << "]" << endl;

        if (pivot == value)
        {
            return true;
        }

        if (start == outResultIndex)
        {
            return false;
        }

        if (pivot > value)
        {
            return binarySearchRecurse(value, start, outResultIndex, a, outResultIndex);
        }
        else 
        {
            return binarySearchRecurse(value, outResultIndex, end, a, outResultIndex);
        }
    }

    bool binarySearch(const long long& value, const std::vector<long long>& a, long& resultIndex)
    {
        PROFILE_BLOCK(g_profilerName, "binarySearch");

        long start = 0;
        long end = a.size() - 1;
        
        resultIndex = 0;
        return binarySearchRecurse(value, start, end, a, resultIndex);
    }

    // takes 30 s (with 100 000, only 0.3s)
    void slow(const vector<long long>& primesVec)
    {
        long resultStreak = 0;
        long long resultPrime = primesVec.back();

        for (long i = primesVec.size() - 1; i != 0; i--)
        {
            PROFILE_BLOCK(g_profilerName, "primeLoop");

            if (resultStreak >= i)
            {
                break;
            } 

            long long prime = primesVec[i];
            long upperLimitIndex;
         
            if (!binarySearch(prime / 2, primesVec, upperLimitIndex))
            {
                upperLimitIndex++;
            }

            //cout << "prime: " << primesVec[i] << ", upperLimit: " << primesVec[upperLimitIndex] << endl;

            for (long j = upperLimitIndex; j > -1; j--)
            {
                PROFILE_BLOCK(g_profilerName, "sumLoop");

                long sum = 0;
                long currentIndex = j;
                long currentStreak = 0;
                
                while (sum < prime
                    && currentIndex > -1
                    && sum + (currentIndex+1) * primesVec[currentIndex] >= prime)
                {
                    PROFILE_BLOCK(g_profilerName, "streakLoop");

                    sum += primesVec[currentIndex];
                    currentIndex--;
                    currentStreak++;

                    //cout << "[" << primesVec[i] << "] sum: " << sum << endl; 
                }

                if (sum == prime && currentStreak > resultStreak)
                {
                    resultStreak = currentStreak;
                    resultPrime = prime;
                }

                //cout << "prime: " << prime << ", found: " << (sum == prime) << ", streak: " << currentStreak << " (longest: " << resultStreak << ")" << endl;
            }
        }

        cout << "longest streak with prime " << resultPrime << " (" << resultStreak << ")" << endl;
    }

      void problem_050()
    {
        time_helpers::SimpleTimer<> timer;

        long long limit = 50000;
        //long long limit = 1000000;
        set<long long> primes;     
        
        math_prime_helpers::getPrimesBelowEratosthenesSieve(limit, primes);
      
        vector<long long> primesVec (primes.size());
        copy(primes.begin(), primes.end(), primesVec.begin());

        long elapsedTimeMs = timer.getElapsed();
        cout << "primes took [ms]: " << elapsedTimeMs << endl;   
        timer.restart();

        slow(primesVec);

        elapsedTimeMs = timer.getElapsed();
        cout << "took [ms]: " << elapsedTimeMs << endl;   

        PROFILER_FRAME_UPDATE(g_profilerName);
  
        auto profiler = time_helpers::Profilers::getProfiler(g_profilerName);
        const size_t bufSize = 1000;
        char buffer[bufSize];
        profiler->writeFormattedOutput(buffer, bufSize);
        cout << buffer << endl;

    }
}