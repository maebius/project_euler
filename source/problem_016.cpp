#include <iostream>
#include <sstream>
#include <boost/multiprecision/cpp_int.hpp>

using namespace std;

namespace project_euler
{
    //! 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
    //! What is the sum of the digits of the number 2^1000?
    // OK -> (1366)
    void problem_016()
    {
        using namespace boost::multiprecision;

        const int limit = 1000;
        
        cpp_int number = pow(cpp_int(2), limit);
        
        stringstream ss;
        ss << number;
        string s(ss.str());

        long long int result = 0;
        for (auto c : s)
        {
            result += c - '0';
        }
        cout << "result (number is " << s.size() << " digits long): " << result << endl;
    }
}