#include <iostream>
#include <string>
#include <algorithm>

#include "MathHelpers.h"

using namespace std;
using namespace MathHelpers;

namespace project_euler
{
    
    //! A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
    //! Find the largest palindrome made from the product of two 3-digit numbers.
    void problem_004()
    {
        const long maxVal = 999;
        long result = -1;
        long resultA = -1;
        long resultB = -1;

        for (long a = maxVal; a > 0; a--)
        {
            if (a < resultB)
            {
                // can early exit as:
                // - b will be <= a
                // - we've already found solution with bigger a and b that is bigger than this a (and all the rest)
                break;
            }

            for (long b = a; b > 0; b--)
            {
                long candinate = a * b;
                bool isP = isPalindrome(candinate);
                if (isP && candinate > result)
                {
                    resultA = a;
                    resultB = b;
                    result = candinate;
                    break;
                }
                else if (candinate <= result)
                {
                    break;
                }
            }   
        }
        cout << "Biggest palindrome for product of 2 numbers with max value each of " << maxVal << ": ";
        if (result > -1)
        {
            cout << result << " (" << resultA << " * " << resultB << ")" << endl; 
        }
        else
        {
            cout << "No result" << endl;
        }
    }
}