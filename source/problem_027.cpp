#include <iostream>
#include <set>
#include <cmath>
#include <map>

#include "mathPrimeHelpers.h"

using namespace std;

namespace project_euler
{
    long getCandidate(int n, int a, int b)
    {
        return abs(n * n + a * n + b);
    }

/*
Euler discovered the remarkable quadratic formula:

n^2+n+41

It turns out that the formula will produce 40 primes for the consecutive integer values 0≤n≤39. 
However, when n=40,40^2+40+41=40(40+1)+41 is divisible by 41, 
and certainly when n=41,41^2+41+41 is clearly divisible by 41.

The incredible formula n^2−79n+1601 was discovered, which produces 80 primes for the consecutive values 0≤n≤79. 
The product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:

n^2+an+b, where |a|<1000 and |b|≤1000

where |n||n| is the modulus/absolute value of n
e.g. |11|=11 and |−4|=4
Find the product of the coefficients, aa and bb, for the quadratic expression that produces 
the maximum number of primes for consecutive values of n, starting with n=0.
*/

    //! -> OK (-59231)
    void problem_027()
    {
        const int startA = -999; // -999
        const int limitA = 1000; // 1000
        
        const int startB = -1000; // -1000
        const int limitB = 1001; // 1001
    
        long maxN = 1000;
        long primeLimit = getCandidate(maxN, limitA, limitB);
        std::set<long long> primes; 

        math_prime_helpers::getPrimesBelowEratosthenesSieve(primeLimit, primes);

        int maxPrimesCount = 0;
        int selA = 0;
        int selB = 0;
        int selN = 0;

        map<int, bool> cache;
        map<int, bool> cacheIsPrime;

        for (int a = startA; a < limitA; a++)
        {
            for (int b = startB; b < limitB; b++)
            {
                int n = 0;
                set<long> foundPrimes;
                while (true) 
                {
                    long candidate = getCandidate(n, a, b);
                    
                    if (primeLimit < candidate)
                    {
                        maxN *= 10;
                        primeLimit = getCandidate(maxN, limitA, limitB);

                        cout << "increase primes to: " << primeLimit << endl;

                        math_prime_helpers::getPrimesBelowEratosthenesSieve(primeLimit, primes);  
                    }

                    if (!cache[candidate])
                    {
                        cache[candidate] = true;
                        cacheIsPrime[candidate] = std::find(primes.begin(), primes.end(), candidate) != primes.end();
                    }

                    if (cacheIsPrime[candidate])
                    {
                        foundPrimes.insert(candidate);
                        //cout << "YES PRIME, a=" << a << ", b=" << b << ", n=" << n << endl;
                    }
                    else
                    {
                        //cout << "NO PRIME, a=" << a << ", b=" << b << ", n=" << n << endl;
                        break;
                    }
                    
                    n++;
                }
                
                int primeCount = foundPrimes.size();
                if (primeCount > maxPrimesCount)
                {
                    maxPrimesCount = primeCount;
                    selA = a;
                    selB = b;
                    selN = n-1;

                    cout << "UPDATE, a=" << selA << ", b=" << selB << ", n=" << selN << ", primes=" << maxPrimesCount << endl;
                }
            }
        }

        cout    << "product: " << (selA * selB) 
                << ", (a=" << selA 
                << ", b=" << selB 
                << ", n=" << selN 
                << ", primes=" << maxPrimesCount
                << ")" 
                << endl;
    }
}