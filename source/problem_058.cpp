#include <iostream>
#include <cassert>
#include "SimpleTimer.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

#include "mathPrimeHelpers.h"

using namespace std;
using namespace math_prime_helpers;

namespace project_euler
{
/*

Starting with 1 and spiralling anticlockwise in the following way, a square 
spiral with side length 7 is formed.

37 36 35 34 33 32 31
38 17 16 15 14 13 30
39 18  5  4  3 12 29
40 19  6  1  2 11 28
41 20  7  8  9 10 27
42 21 22 23 24 25 26
43 44 45 46 47 48 49

It is interesting to note that the odd squares lie along the bottom right 
diagonal, but what is more interesting is that 8 out of the 13 numbers lying 
along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.

If one complete new layer is wrapped around the spiral above, a square spiral 
with side length 9 will be formed. If this process is continued, what is the 
side length of the square spiral for which the ratio of primes along both 
diagonals first falls below 10%?
*/

	// -> OK (26241)
	void problem_058()
	{
		time_helpers::SimpleTimer<> timer;
		
		const float ratioLimit = 0.1f;
		
		// TODO: this is wasteful, takes 5s 
		vector<bool> isPrime;
		const long primeLimit = 1000000000;
		getPrimesBelowEratosthenesSieveVector(primeLimit, isPrime); 

		long primesOnDiagonal = 0;
		long allCount = 1;

		float ratio = 1.0f;

		long current = 1;
		long add = 0;
		
		while (ratio >= ratioLimit)
		{
			add += 2;

			for (int i = 0; i < 4; i++)
			{
				current += add;

				assert(current < isPrime.size());

				if (isPrime[current])
				{
					primesOnDiagonal++;
				}
			}
			allCount += 4;
			ratio = 1.0f * primesOnDiagonal / allCount;

 			/* cout	<< "cur: " << current 
					<< ", add: " << add << ", prime/all: "
					<< primesOnDiagonal << " / " << allCount
					<< " (" << ratio << ")" << endl;   */
		}

		cout	<< "sidelength: " << (add + 1)
				<< ", current: " << current
				<< ", prime/all: " << primesOnDiagonal << " / " << allCount
				<< " (" << ratio << ")" << endl; 

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}