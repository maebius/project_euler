#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>
#include <ctype.h>

using namespace std;

namespace project_euler
{
    using namespace std;

    std::string replaceAllOccurences(std::string input, const std::string& replace, const std::string& with)
    {
        auto r = input.find(replace);
        while (r != std::string::npos)
        {
            input.replace(r, r + replace.length(), with);
            r = input.find(replace);
        }
        return input;
    }

    int getNameScore(const std::string& name)
    {
        int result = 0;
        for (auto c : name)
        {
            result += tolower(c) - 'a' + 1;
        }
        return result;
    }


    /*
    Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
*/

    //! -> OK (871198282)
    void problem_022()
    {
        fstream fin("problem_022_names.txt", fstream::in);
        string s;

        vector<string> names;

        string r{"\""};
        string w{""};
        while(getline(fin, s, ','))
        {
            string name{replaceAllOccurences(s, r, w)};
            names.push_back(name);
        }
        sort(names.begin(), names.end());

        long totalScore = 0;
        for (int i = 0; i < names.size(); i++) 
        {
            int nameScore = getNameScore(names[i]);

            totalScore += (i+1) * nameScore;

            //cout << i << ": " << names[i] << ": " << nameScore << endl;        
        }

        cout << "count: " << names.size() << endl;
        cout << "total score: " << totalScore << endl;
    }
}