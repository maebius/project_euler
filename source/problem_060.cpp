#include <iostream>
#include <limits>
#include <numeric>
#include "SimpleTimer.h"

#include "mathPrimeHelpers.h"
#include "mathCombinatorics.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

using namespace std;
using namespace math_prime_helpers;
using namespace math_combinatorics;

namespace project_euler
{
	bool isPairConcatenatedPrimes(std::vector<bool>& primes
		, long long first, long long second, bool& result)
	{
		std::string f = std::to_string(first);
		std::string s = std::to_string(second);

		long primeLimit = primes.size();

		long n1 = std::stol(f + s);
		if (n1 >= primeLimit)
		{
			return false;
		}
		//cout << f << " . " << s << " = " << n1 << endl;
		if (!primes[n1])
		{
			result = false;
			return true;
		}
		long n2 = std::stol(s + f);
		if (n2 >= primeLimit)
		{
			return false;
		}
		//cout << s << " . " << f << " = " << n2 << endl;
		if (!primes[n2])
		{
			result = false;
			return true;
		}
		result = true;
		return true;
	}

	/* bool areConcatenatedPrimes(std::vector<bool>& primes
		, std::vector<long long> candidates)
	{
		int s = candidates.size();
		for (int i = 0; i < s; i++)
		{
			long long first = candidates[i];
			for (int j = 0; j < i; j++)
			{
				long long second = candidates[j];
				if (!isPairConcatenatedPrimes(primes, first, second))
				{
					return false;
				}
			}
			for (int j = i+1; j < s; j++)
			{
				long long second = candidates[j];
				if (!isPairConcatenatedPrimes(primes, first, second))
				{
					return false;
				}
			}
		}
		return true;
	} */

	long getNextPrime(std::vector<bool>& primes, long start)
	{
		long s = primes.size();
		for (long i = start; i < s; i++)
		{
			if (primes[i])
			{
				return i;
			}
		}
		return -1;
	}

	struct PrimePair
	{
		long first;
		long second;

		bool operator==(const PrimePair& rhs)
		{
			return this->first == rhs.first && this->second == rhs.second;
		}

		friend std::ostream& operator<<(std::ostream& stream, const PrimePair& p)
		{
			stream << p.first << " " << p.second;
			return stream;
		}
	};

	bool hasEnoughPairs(std::vector<PrimePair>& pairs
		, std::set<long>&  primeCandidates
		, std::vector<std::vector<long>>& permutations
		, const int count
		, std::vector<long>& result
		, long& smallestSum)
	{
		// TODO

		// . muodosta kaikista "primeCandinatesista" "count":in permutaatiot
		// . tätä joukkoa vasten, jokaisen ryhmän sisällä muodosta kaikki parit
		// . katso löytyykö se pari "pairs":ista, jos ei, ryhmä epävalidi
		// . jos kaikki löytyy, validi ryhmä -> laske summa 


		// TODO - tässä: (TÄSSÄ IDEAA, mutta ei lopullista)
		// . cacheta ne permutationit joille tulee false
		// . eli tämä "permutations" tulee tänne parametriksi
		// . kutsuja lisää siihen aina uuden primen aiheuttavat kombot
		// . alla oleva looppi merkkaa toiseen rakenteeseen
		//   jos !valid että sillä kombolla ei tuu mitään
		// . tällä voidaan skipata heti permutaation entry täsäs
		//   -> voitaisko se poistaakin jotenkin koko rakenteesta???

		smallestSum = std::numeric_limits<long>::max();

		for (int i = 0; i < permutations.size(); i++)
		{
			std::set<long> p(permutations[i].begin(), permutations[i].end());

			long s = std::accumulate(p.begin(), p.end(), 0);
			
			if (s >= smallestSum)
			{
				break;
			} 

			std::vector<std::vector<long>> pp = getPermutations(
				p, 2);

			bool valid = true;
			for (int j = 0; j < pp.size(); j++)
			{ 
				PrimePair c {pp[j][0], pp[j][1]};
				auto res = std::find(pairs.begin(), pairs.end(), c);
				if (res == std::end(pairs))
				{
					// TODO: voisko tässä vaan poistaa parin ja primet
					// veks? 

					//cout << "not valid: " << c << endl;
					valid = false;
					break;
				}
			}
			if (valid)
			{
				result = { p.begin(), p.end() };
				smallestSum = s;
			}
		}

		return smallestSum < std::numeric_limits<long>::max();
	}

	bool hasPermutation(std::vector<std::vector<long>>& permutations
		, std::vector<long>& newElement)
	{
		int s = newElement.size();
		for (int i = 0; i < permutations.size(); i++)
		{
			std::vector<long>& p = permutations[i];
			for (int j = 0; j < s; j++)
			{
				if (p[j] != newElement[j])
				{
					return false;
				}
			}
		}
		return true;
	}

	void addPermutations(std::vector<std::vector<long>>& permutations
		, long newElement)
	{
		int oldPermCount = permutations.size();
		int elemCount = permutations[0].size();
		
		for (int i = 0; i < oldPermCount; i++)
		{
			for (int j = 0; j < elemCount; j++)
			{
				std::vector<long> cand{ permutations[i] };
				cand.erase(cand.begin() + j);
				cand.push_back(newElement);

				// TODO: sooo slow, get rid of this somehow?
				if (!hasPermutation(permutations, cand))
				{
					cout << "add perm: ";
					for_each(cand.begin(), cand.end(), [](long n) { cout << n << " "; });
					cout << endl;

					permutations.push_back(cand);
				}
			}
		}
	}

	void getConcatenatedPrimes(std::vector<bool>& primeCheck
		, std::vector<long>& result
		, const int targetCount)
	{
		// miten valita kandinaatit?
	
		// . mene primejä eteenpän
		// . tallenna aina parit jotka alkaen ekasta N:n täyttävät propertyn
		// . pidä jokaiselle primelle hashia, joka laskee kuinka monessa parissa mukana
		// . kun näitä primejä joille pareja on >= 4, on >= 5, voidaan ruveta tutkimaan:
		// .. ota alkujoukoksi kaikki osalliset primet
		// .. muodosta niistä kaikki 5 kombinaatiot
		// .. katso onko ne parien joukossa -> jos joo, laske summa

		long currentPrime = getNextPrime(primeCheck, 0);
		std::vector<long> primes;
		std::vector<PrimePair> pairs;
		std::map<long, int> partOfPairCount;

		std::set<long> primeCandidates;
		
		long maxPrime = primeCheck.size();
		long minCount = targetCount - 1;

		bool isConcat = false;

		long smallestSum = std::numeric_limits<long>::max();

		bool notFound = true;

		std::vector<std::vector<long>> permutations;		
		
		while (notFound && currentPrime < maxPrime && currentPrime != -1)
		{
			for (int i = 0; i < primes.size(); i++)
			{				
				if (!isPairConcatenatedPrimes(primeCheck
						, currentPrime, primes[i], isConcat))
				{
					currentPrime = -1;
					break;
				}

				if (isConcat)
				{
					pairs.push_back( { primes[i], currentPrime });

					partOfPairCount[primes[i]] = 
						partOfPairCount[primes[i]] + 1;
					partOfPairCount[currentPrime] = 
						partOfPairCount[currentPrime] + 1;
					
					if (partOfPairCount[primes[i]] >= minCount)
					{
						// TÄÄ ON ROSKAA TODNÄK
						if (primeCandidates.find(primes[i]) == primeCandidates.end())
						{
							if (permutations.empty())
							{


							}
							else
							{
								addPermutations(permutations, primes[i]);
							}
							primeCandidates.insert(primes[i]);
						}
					}
					if (partOfPairCount[currentPrime] >= minCount)
					{
						// TÄNNE PITÄIS DUPLIKOIDA YLLÄ OLEVA ROSKA

						primeCandidates.insert(currentPrime);
					}
					if (primeCandidates.size() >= targetCount)
					{

						// TODO: pitää saada nopeemmaks
						// . cachettaa kokonaisuudessaa edellisiä tuloksia ...
						// . nyt lasketaan uudestaan ja uudestaan samaa
						// . pitää saada merkatuksi sellaset pariyhdistelmät
						// jotka ei täytä ehtoa -> voi skipata heti

						long sum;
						std::vector<long> members;
						bool enough = hasEnoughPairs(pairs // liian hidas
							, primeCandidates
							, permutations
							, targetCount
							, members
							, sum);
					
						/* cout	<< "curPrime: " << currentPrime
								<< ", candidates: " << primeCandidates.size()
								<< ", primes: " << primes.size() << endl; */

						if (enough && sum < smallestSum)
						{
							notFound = false;
							smallestSum = sum;
							result = members;
						}
					}
				}
			}
			if (currentPrime != -1)
			{
				primes.push_back(currentPrime);
				currentPrime = getNextPrime(primeCheck, currentPrime+1);
			}
		}

		//sort(primeCandidates.begin(), primeCandidates.end());
		//for_each(primeCandidates.begin(), primeCandidates.end(), [](long long n) { cout << n << " "; });
		//cout << endl;
	}
	

	void problem_060()
	{
		time_helpers::SimpleTimer<> timer;

		const long MAX_PRIME = 10000000;
		std::vector<bool> primes;

		getPrimesBelowEratosthenesSieveVector(MAX_PRIME, primes);

		// . funkka ottaa parametrina luvun kuinka monta pitää löytää
		// . ... lisäksi kuljetetaan pienintä summaa mukana, ni voidaan skipata
		// . lähtee ekasta primestä, hakee seuraavan joka konkatenoi
		// . siitä rekursiivisesti seuraava kunnes limitti täyteen

		std::vector<long> result;
		int targetCount = 3;
		
		getConcatenatedPrimes(primes, result, targetCount);

		for_each(result.begin(), result.end(), [](long n) { cout << n << " "; });
		cout << endl;

		// ...

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}