#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
    // -> OK (983), length = 982
    void process()
    {
        const int start = 2;
        const int last = 999;

        long longest = 0;
        int d;

        int maxDecimals = 100;
        int limitDecimals = maxDecimals / 4;

        std::ostringstream ss;
     
        for (int i = start; i <= last; i++)
        {
            long whole;
            vector<int> decimals;

            if (!math_natural_helpers::divide(1, i
                , whole
                , decimals
                , maxDecimals))
            {
                ss.str("");
                std::for_each(decimals.begin(), decimals.end(), [&ss](const int& d) { ss << d; } );

                string candidate{ss.str()};

                // NOTE! this routine is very slow ...
                //string occurence = math_natural_helpers::getLongestRecurrence(candidate);
               // this is much faster, but might not be "exact"
               string occurence = math_natural_helpers::getLongestTailRecurrence(candidate);
     
                unsigned long l = occurence.length();
            
                if (l > longest)
                {
                    //cout << "[" << i << "] - " << l << ": " << occurence << endl;

                    longest = l;
                    d = i;

                    if (longest > limitDecimals)
                    {
                        maxDecimals *= 2;
                        limitDecimals = maxDecimals / 4;
                    }
                }
            }
        }

        cout << "RESULT - [" << d << "] - length: " << longest << endl;
    }

    void problem_026()
    {  
        // NOTE! really can't use boost::multiprecision, as cpp_dec_float_100 has accuracy of 100 digits, and answer almost 1000
        process(); 
    }
}