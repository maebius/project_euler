#include "mathNaturalHelpers.h"
#include "mathPrimeHelpers.h"
#include "MathHelpers.h"
#include <iostream>
#include <cmath>
#include <set>
#include <numeric>
#include <cassert>
#include <regex>

using namespace std;

namespace math_natural_helpers
{
    long getNaturalDivisorCount(long long int number, std::set<long long>& primes)
    {
        assert(number > 0);
        vector<pair<long, int>> primeFactors;
        
        //math_prime_helpers::getPrimeFactorsRecursive(number, primeFactors);
        math_prime_helpers::getPrimeFactors(number, primeFactors, primes);

        //cout << "primeFactors: " << primeFactors.size() << ", primes: " << primes.size() << endl;

        long result = 1;
        for (auto p : primeFactors)
        {
            //cout << "[" << number << "] " << p.first << "^" << p.second << " (" << (p.first*p.second) << ")" << endl;
            result *= (1 + p.second);
        }
       
        return result;
    }


    void getNaturalDivisorRecursion(long current, long index, const vector<pair<long, int>>& primeFactors, std::set<long long>& divisors)
    {
        pair<long, int> currentFactor = primeFactors[index];
        bool isLast = index == primeFactors.size() - 1;
        
        long m = current;
        for (int i = 0; i <= currentFactor.second; i++)
        {
            if (isLast)
            {
                divisors.insert(m);
            }
            else 
            {
                getNaturalDivisorRecursion(m, index + 1, primeFactors, divisors);
            }
            
            m *= currentFactor.first;
        }
    }

    void getNaturalDivisors(long long int number, std::set<long long>& primes, std::set<long long>& divisors)
    {
        assert(number > 0);
        if (number == 1)
        {
            divisors.insert(1);
            return;
        }

        vector<pair<long, int>> primeFactors;
        
        math_prime_helpers::getPrimeFactors(number, primeFactors, primes);

        //cout << "primeFactors: " << primeFactors.size() << ", primes: " << primes.size() << endl;

        getNaturalDivisorRecursion(1, 0, primeFactors, divisors);
    }

    long long int getNthTriangleNumber(long long int n)
    {
        return (1 + n) * n / 2;
    }

    long getSumOfProperDivisors(long number, std::set<long long>& primes)
    {
        assert(number > 0);

        set<long long> divisors;
        math_natural_helpers::getNaturalDivisors(number, primes, divisors);
        //cout << "divisors for " << number << ": "; for (auto d : divisors) cout << d << " "; cout << endl;
        long sum = std::accumulate(divisors.begin(), divisors.end(), -number);
        //cout << "sum: " << sum << endl;
        return sum;
    }

    void getAbundantNumbers(const long start, const long last, std::vector<int>& result)
    {
        set<long long> primes;
        math_prime_helpers::getPrimesBelowEratosthenesSieve(last+1, primes);

        for (int i = start; i <= last; i++)
        {
            long sum = getSumOfProperDivisors(i, primes);
            //cout << i << ": " << sum << endl;
            if (sum > i)
            {
                result.push_back(i);
            }
        }
    }

    void power(const int base
        , const int power
        , std::vector<short>& outDecals)
    {
        if (power == 0)
        {
            outDecals.push_back(1);
            return;
        }
        int initial = base;
        while (initial != 0)
        {
            outDecals.push_back(initial % 10);
            initial /= 10;
        }

        for (int e = 1; e < power; e++)
        {
            int carryOver = 0;
            for (int i = 0; i < outDecals.size(); i++)
            {
                int current = outDecals[i] * base + carryOver;
                outDecals[i] = current % 10;
                carryOver = current / 10;
            }
            while (carryOver != 0)
            {
                outDecals.push_back(carryOver % 10);
                carryOver /= 10;
            }
        }
    }

    bool divide(const long numerator
        , const long denumerator
        , long& outWhole
        , std::vector<int>& outDecimals
        , const int stopAtDecimal)
    {
        if (numerator > denumerator)
        {
            outWhole = numerator / denumerator;
        }
        int decimalCount = 0;
        long remainder = numerator % denumerator;
        while (remainder != 0)
        {
            if (decimalCount == stopAtDecimal)
            {
                return false;
            }
            remainder *= 10;
            outDecimals.push_back(remainder / denumerator);
            remainder %= denumerator;
            
            decimalCount++;
        }

        return true;
    }

    long getNonOverlappingRecurrences(const std::string& where, const std::string& what)
    {
        long occurrences = 0;
        std::string::size_type pos = 0;
        while ((pos = where.find(what, pos )) != std::string::npos) 
        {
            ++occurrences;
            pos += what.length();
        }
        return occurrences;
    }

    std::string getLongestTailRecurrence(const std::string& value)
    {
        std::string rev{value};
        std::reverse(rev.begin(), rev.end());

        std::string result;
        for (long c = 1; c <= rev.length() / 2; c++)
        {
            if (rev[0] == rev[c])
            {
                std::string first { rev.substr(0, c) };
                std::string second { rev.substr(c, c) };

                //cout << "1st: " << first << ", 2nd: " << second << endl;

                if (first == second)
                {
                    long fl = first.length();
                    long ll = result.length();

                    if (ll >= fl)
                    {
                        continue;
                    }

                    //cout << "longest: " << ll << ", this: " << fl << endl;

                    // if we have already a result, need to check that this is not just a "multiply" of something of same length
                    if (ll > 0 && (fl % ll == 0))
                    {
                        long fit = fl / ll;
                        long found = getNonOverlappingRecurrences(first, first.substr(0, ll));

                        //cout << "fit: " << fit << ", found: " << found << endl;

                        if (fit == found)
                        {
                            //cout << "skip: " << first << endl;
                            continue;
                        }
                    }

                    result = first;
                }
            }
        }
        std::reverse(result.begin(), result.end());
        return result;
    }

    // NOTE! This routine is very slow
    std::string getLongestRecurrence(const std::string& value)
    {
        //cout << "getLongestReoccurence: " << value << endl;

        std::string longest;
        for (long c = 0; c <= value.length(); c++)
        {
            if (longest.length() >= value.length() - c)
            {
                break;
            }

            for (long i = c + 1; i <= value.length(); i++)
            {
                if (longest.length() >= value.length() - i)
                {
                    break;
                }

                // stop to analyze whenever we hit the same char as the first one
                if (value[c] == value[i])
                {
                    // are the strings up to this point, and the same length string after this, the same?
                    std::string first { value.substr(c, i - c) };
                    std::string second { value.substr(i, i - c) };
                    
                    // TODO: by analysing "first" here and checking if it has reoccurences, we could
                    // just skip at this pois all below if it has ...

                    //cout << "[" << c << "] 1st: " << first << ", [" << i << "] 2nd: " << second << endl;

                    if (first == second)
                    {
                        long fl = first.length();
                        long ll = longest.length();

                        if (ll >= fl)
                        {
                            continue;
                        }

                        //cout << "longest: " << ll << ", this: " << fl << endl;

                        // if we have already a result, need to check that this is not just a "multiply" of something of same length
                        if (ll > 0 && (fl % ll == 0))
                        {
                            long fit = fl / ll;
                            long found = getNonOverlappingRecurrences(first, first.substr(0, longest.length()));
 
                            //cout << "fit: " << fit << ", found: " << found << endl;

                            if (fit == found)
                            {
                                //cout << "skip: " << first << endl;
                                continue;
                            }
                        }
                        // need to make sure that the reoccurence continues after
                        bool valid = true;
                        long j = i + fl;
                        long k = c;
                        while (j < value.length())
                        {
                            //cout << "v[" << k << "] = " << value[k] << ", v[" << j << "] = " << value[j] << endl;
                               
                            if (value[k] != value[j])
                            {
                                valid = false;
                                break;
                            }
                            j++;
                            k++;
                            if (k == c + first.size())
                            {
                                k = c;
                            }
                        }
                        
                        if (valid)
                        {
                            //cout << "UPDATE: " << first << endl;

                            longest = first;
                        }
                    }
                }
            }
        }
        return longest;
    }    

    long long getPentagonal(long long n)
    {
        return n * ((3 * n) - 1) / 2;
    }

    long long getPentagonalIndex(long long n)
    {
        // solving above "getPentagonal", and requiring that D is a natural number,
        // and the result a whole number (so, D uneven), gives us:
        long long d = 1 + 24 * n;
        
        long long s;
        
        //cout << "getPentagonalIndex(n=" << n << ") - d: " << d << endl;

        if (!MathHelpers::isSqrtInteger(d, s))
        {
            return -1;
        }
        s = s + 1;

        if (s % 6 != 0)
        {
            return -1;
        }

        return s / 6;
    }

    long long getHexagonal(long long n)
    {
        return (2 * n * n) - n;
    }

    long long getHexagonalIndex(long long n)
    {
        // same idea as in "getPentagonalIndex"
        long long d = 1 + 8 * n;
        long long s;
        
        if (!MathHelpers::isSqrtInteger(d, s))
        {
            return -1;
        }
        s = s + 1;
        if (s % 4 != 0)
        {
            return -1;
        }

        return s / 4;
    }

    long long getTriangle(long long n)
    {
        return (n * n + n)/2;
    }
}