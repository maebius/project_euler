#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <locale>

using namespace std;

namespace project_euler
{
/*
The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); 
so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position 
and adding these values we form a word value. For example, 
the word value for SKY is 19 + 11 + 25 = 55 = t_10. If the word value is a triangle number 
then we shall call the word a triangle word.

Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly 
two-thousand common English words, how many are triangle words?
*/

    //! -> OK (162)
    short getValue(char c)
    {
        return c - 'A' + 1;
    }

    static int g_biggestTriangleNumberN = 1;
    static vector<int> g_triangleNumbers { 1 };
    
    bool isTriangleNumber(int value)
    {
        if (g_triangleNumbers[g_triangleNumbers.size()-1] < value)
        {
            int newNumber;
            do 
            {
                g_biggestTriangleNumberN++;
                newNumber = (g_biggestTriangleNumberN * (g_biggestTriangleNumberN + 1)) / 2;
                g_triangleNumbers.push_back(newNumber);
            }
            while (newNumber < value);
        }
        return std::find(g_triangleNumbers.begin(), g_triangleNumbers.end(), value) != g_triangleNumbers.end();
    }

    void problem_042()
    {
        const string filename{"p042_words.txt"};

        fstream fin(filename, fstream::in);
        
        char c;
        string word;
        int wordCount = 0;
        int wordValue = 0;
        int triangleWordCount = 0;
        while (fin >> noskipws >> c) 
        {
            if (c == '"')
            {
                if (word.size() > 0)
                {
                    wordCount++;
                    cout << word << ": " << wordValue << endl;
                    if (isTriangleNumber(wordValue))
                    {
                        triangleWordCount++;
                    }
                }

                word = "";
                wordValue = 0;
            }
            else if (isalpha(c))
            {
                word += c;
                wordValue += getValue(c);
            }
        }
        
        fin.close();

        cout << "[" << wordCount << "] -> " << triangleWordCount << endl;
    }
}