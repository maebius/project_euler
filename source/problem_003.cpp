#include <iostream>
#include <vector>
#include <algorithm>

#include "mathPrimeHelpers.h"

using namespace std;

namespace project_euler
{
    //! The prime factors of 13195 are 5, 7, 13 and 29.
    //! What is the largest prime factor of the number 600851475143 ?
    // -> OK (6857)
    void problem_003()
    {
        const long number = 600851475143;
        //const long number = 13195;
        vector<pair<long, int>> result;

        math_prime_helpers::getPrimeFactorsRecursive(number, result);

        sort(result.begin(), result.end(), [](const pair<long, int>& p1, const pair<long, int>& p2) 
        { 
            return p1.first > p2.first;
        });

        cout << "biggest prime factor for " << number << " is " << result[0].first << endl;
        cout << "all factors: " << endl;
        for (auto n : result) { cout << n.first << " "; }
        cout << endl;
    }
}