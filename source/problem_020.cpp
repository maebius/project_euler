#include <iostream>
#include <vector>
#include <numeric>

#include "mathCombinatorics.h"

using namespace std;

namespace project_euler
{
    /*
    n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
*/
    //! - OK (648)
    void problem_020()
    {
        const long number = 100;
        vector<int> result;
        math_combinatorics::getFactorialDigits(number, result);

        int sum = accumulate(result.begin(), result.end(), 0);

        cout << "sum for digits of factorial " << number << ": " << sum << endl; 
    }
}