#include <iostream>
#include <map>
#include <cmath>

#include "MathHelpers.h"

using namespace std;

namespace project_euler
{
/*
If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?
*/
    // -> OK (840)
    void problem_039()
    {
        const int limit = 1000;

        map<int, int> squares;      
        map<int, int> perimeters;      
        
        for (int i = 0; i <= limit; i++)
        {
            squares[i] = i * i;
        }

        for (int i = 1; i <= limit; i++)
        {
            for (int j = i; j <= limit; j++)
            {
                if (i + 2 * j >= limit)
                {
                    break;
                }

                long long h;
                if (MathHelpers::isSqrtInteger(squares[i] + squares[j], h))
                {
                    int p = i + j + h;
                    if (p <= limit)
                    {
                        int newCount = perimeters[p] + 1;
                        perimeters[p] = newCount;

                        //cout << i << " " << j << " " << (int) h << endl;
                    }
                }
            }   
        }

        int biggestCount = 0;
        int chosenPerimeter = 0;
        for (auto pair : perimeters)
        {
            if (biggestCount < pair.second)
            {
                biggestCount = pair.second;
                chosenPerimeter = pair.first;
            }
        }
        const int testPerimeter = 120;
        cout << "test [" << testPerimeter << "]: " << perimeters[testPerimeter] << endl;

        cout << "biggest    [" << chosenPerimeter << "]: " << biggestCount << endl;
    }
}