#include <iostream>

using namespace std;

namespace project_euler
{
    /*
    Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
*/

    //! -> OK (669171001)
    void problem_028()
    {
        const int rectSize = 1001;
        const int limit = rectSize * rectSize;

        int val = 1;
        long sum = val;

        int delta = 2;
        while (val < limit)
        {
            for (int i = 0; i < 4; i++)
            {
                val += delta;
                sum += val;
            }
            delta += 2;
        }
        
        cout << "rectSize: " << rectSize << ", result sum: " << sum << endl;
    }
}