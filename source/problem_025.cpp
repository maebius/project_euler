#include <iostream>

using namespace std;

namespace project_euler
{
    /*
    The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
*/
    //! -> OK (4782)
    void problem_025()
    {
        const int digitLimit = 1000;

        double fn = 1.0;
        double fn1 = 1.0;

        long currentDigit = 1;

        long currentFibonacci = 2;

        while (currentDigit < digitLimit)
        {
            double f = fn + fn1;
            while (f >= 10.0)
            {
                // NOTE! This is not accurate with big limits, as double is not indefinitely accurate ...
                currentDigit++;
                f /= 10.0;
                fn1 /= 10.0;
            }
            fn = fn1;
            fn1 = f;
            currentFibonacci++;
        }

        cout << currentFibonacci << ". fibonacci is over " << digitLimit << " digits" << endl;
    }
}