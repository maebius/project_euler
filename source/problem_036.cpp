#include <iostream>
#include <vector>
#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
/*
The decimal number, 585 = 1001001001(2) (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)
*/
    //! -> OK (872187)
    bool isPalindrome(const std::vector<short>& numbers)
    {
        size_t s = numbers.size();
        bool even = s % 2 == 0;
        size_t hiIndex = even ? (s / 2) : ((s / 2) + 1);
        size_t loIndex = even ? (hiIndex - 1) : (hiIndex - 2);

        while (loIndex != -1)
        {
            //cout << "hi (" << hiIndex << "): " << numbers[hiIndex] << ", lo (" << loIndex << "): " << numbers[loIndex] << endl;

            if (numbers[hiIndex] != numbers[loIndex])
            {
                return false;
            }
            loIndex--;
            hiIndex++;
        } 
        return true;
    }

   
    void problem_036()
    {
        const int start = 0;
        const int limit = 1000000;

        long long resultSum = 0;
        long resultCount = 0;

        for (int i = start; i < limit; i++)
        {
            if (i % 2 == 0)
            {
                continue;
            }

            vector<short> decimals;
            math_natural_helpers::getDigitsForBase(i, 10, decimals);
            if (!isPalindrome(decimals))
            {
                continue;
            }
            vector<short> binary;
            math_natural_helpers::getDigitsForBase(i, 2, binary);
            if (isPalindrome(binary))
            {
                resultSum += i;
                resultCount++;
            }
        }

        cout << "[" << resultCount << "]: " << resultSum << endl;
    }
}