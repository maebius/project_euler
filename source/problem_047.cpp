#include <iostream>
#include <utility>
#include "mathPrimeHelpers.h"
#include "mathNaturalHelpers.h"
#include "SimpleTimer.h"

using namespace std;

namespace project_euler
{
    void printFactorisation(const long& i, const std::vector<pair<long, int>>& result)
    {
        cout << i << " [" << result.size() << "] = ";
        for_each(result.begin(), result.end(), [](const pair<long, int>& p)
        {
            cout << p.first << "^" << p.second << "\t* ";
        });
        cout << " 1" << endl;
    }

/*
The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?
*/
    //! -> OK (134043)
    void problem_047()
    {
        time_helpers::SimpleTimer<> timer;

        const long consecutives = 4;

        set<long long> primes;
        long long limit = 1000;     
        
        long long start = 1;
        math_prime_helpers::getPrimesBelowEratosthenesSieve(limit, primes);
        auto p = primes.begin();
        for (int i = 0; i < consecutives && p != primes.end(); i++, p++)
        {
            start *= *p;
        }

        cout << "looking " << consecutives << " numbers with as many distintive primes, initial start: " << start << endl;

        long i = start + consecutives - 1;
          
        int consecutiveCount = 0;

        while (true)
        {
            vector<pair<long, int>> result;
            math_prime_helpers::getPrimeFactors(i, result, primes);

            if (result.size() == consecutives)
            {
                consecutiveCount++;

                //cout << "[" << i << "], consectutives: " << consecutiveCount << endl;

                if (consecutiveCount == consecutives)
                {
                    cout << i  << endl;
                    break;
                }

                i--;
            }
            else
            {
                consecutiveCount = 0;
                i += consecutives;
            }
        }

        long elapsedTimeMs = timer.getElapsed();
        cout << "took [ms]: " << elapsedTimeMs << endl;   
    }
}