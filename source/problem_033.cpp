#include <iostream>
#include <utility>
#include <vector>

using namespace std;

namespace project_euler
{
/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting 
to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, 
and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
*/
    //! -> OK (100)
    struct Result
    {
        int srcN;
        int srcD;

        int trgN;
        int trgD;
    };

    void problem_033()
    {
        vector<Result> results;

        for (int n = 11; n < 100; n++)
        {
            int nr = n % 10;
            if (nr == 0)
            {
                continue;
            }
            int nw = n / 10;

            for (int d = 11; d < 100; d++)
            {
                if (n >= d)
                {
                    continue;
                }

                int dr = d % 10;
                if (dr == 0)
                {
                    continue;
                }
                int dw = d / 10;

                double v = 1.0 * n / d;
                if (nr == dr)
                {
                    if (1.0 * nw / dw == v)
                    {
                        results.push_back(Result{n, d, nw, dw});
                        continue;
                    }
                }
                if (nw == dw)
                {
                    if (1.0 * nr / dw == v)
                    {
                        results.push_back(Result{n, d, nr, dw});
                        continue;
                    }
                }
                if (nr == dw)
                {
                    if (1.0 * nw / dr == v)
                    {
                        results.push_back(Result{n, d, nw, dr});
                        continue;
                    }
                }
                if (nw == dr)
                {
                    if (1.0 * nr / dw == v)
                    {
                        results.push_back(Result{n, d, nr, dw});
                        continue;
                    }
                }
            }
        }
        
        int i = 1;
        int n = 1;
        int d = 1;
        for (auto r : results)
        {
            cout << "[" << i++ << "] " << r.srcN << " / " << r.srcD << " => " << r.trgN << " / " << r.trgD << endl;
            n *= r.trgN;
            d *= r.trgD;
        }
        cout << "result: " << n << " / " << d << endl;
        
        if (d % n == 0)
        {
            cout << "final: 1 / " << (d / n) << endl; 
        }
    }
}