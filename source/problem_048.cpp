#include <iostream>
#include <vector>
#include "SimpleTimer.h"
#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
/*
The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
*/
    //! -> OK (9110846700)

    void addDecalsFromTo(std::vector<short>& from, std::vector<short>& to)
    {
        if (to.size() < from.size())
        {
            to.resize(from.size());
        }
        short carryOver = 0;
        size_t i = 0;
        for ( ; i < from.size(); i++)
        {
            short c = carryOver + from[i] + to[i];
            carryOver = c / 10;
            to[i] = c % 10;
        }
        while (carryOver != 0)
        {
            if (i == to.size())
            {
                to.push_back(0);
            }
            short c = carryOver + to[i];
            carryOver = c / 10;
            to[i] =  c % 10;

            i++;
        }
    }

    void problem_048()
    {
        time_helpers::SimpleTimer<> timer;

        vector<short> sum;

        const int start = 1;        
        const int last = 1000;

        for (int i = start; i <= last; i++)
        {
            vector<short> decals;
            math_natural_helpers::power(i, i, decals);
            addDecalsFromTo(decals, sum);
        }

        const int digits = 10;

        vector<short> result (digits);
        copy(sum.begin(), sum.begin() + digits, result.begin());

        reverse(result.begin(), result.end());
        for (auto d : result) { cout << d; } cout << endl;

        long elapsedTimeMs = timer.getElapsed();
        cout << "took [ms]: " << elapsedTimeMs << endl;   
    }
}