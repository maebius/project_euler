#include <iostream>
#include <set>
#include "SimpleTimer.h"
#include "mathPrimeHelpers.h"
#include "mathNaturalHelpers.h"
#include "mathCombinatorics.h"

using namespace std;

namespace project_euler
{
/*
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, 
is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit 
numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, 
exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
*/
    //! -> OK (296962999629)

    set<long long> g_primes;
        
    bool isValid(const std::vector<short>& v)
    {
        //std::for_each(v.begin(), v.end(), [](const short s) { cout << s; }); cout << endl;

        size_t s = v.size();

        if (s > 4)
        {
            return false;
        }
        if (s < 4)
        {
            return true;
        }

        int n = v[0] * 1000 + v[1] * 100 + v[2] * 10 + v[3];
        
        //cout << "isValid - n: " << n << endl;

        if (n < 1000)
        {
            return false;
        }

        return std::find(g_primes.begin(), g_primes.end(), n) != g_primes.end();
    }

    void problem_049()
    {
        /*
        std::set<int> numbers { 102, 75, 23, 11, 7, 4, 1 };
        auto permutations = math_combinatorics::getPermutations<int>(numbers, 3);
        for (auto p : permutations)
        {
            for (auto n : p)
            {
                cout << n << " ";
            }   
            cout << endl;
        }             

        auto should = math_combinatorics::getBinomialCoefficient(3, numbers.size());
        cout << "was: " << permutations.size() << ", should: " << should << endl;
        return;*/

        time_helpers::SimpleTimer<> timer;

        set<string> concatenations;

        long long smallest = 1000;
        long long limit = 10000;
        math_prime_helpers::getPrimesBelowEratosthenesSieve(limit, g_primes);

        for (auto prime = g_primes.rbegin(); prime != g_primes.rend() && *prime > smallest; prime++)
        {
            //cout << "checking prime: " << *current << endl;

            vector<short> digits;
            math_natural_helpers::getDigitsForBase(*prime, 10, digits);
  
            //cout << "s = " << digits.size() << endl;

            //auto results = 
            //    math_combinatorics::getPandigitalsWithCondition<unsigned short>(digits, isValid);
            auto results = 
                math_combinatorics::getPandigitalsWithCondition<short>(digits, isValid);

            if (results.size() > 1)
            {
                //cout << "for prime: " << *current << ", valid permutations: " << results.size() << endl;

                set<int> numbers;
                for (auto v : results)
                {
                    int n = v[0] * 1000 + v[1] * 100 + v[2] * 10 + v[3];
                    numbers.insert(n);
                }
                
                if (numbers.size() > 2)
                {
                    //cout << "prime: " << *prime << ", prime permutations: " << numbers.size() << endl;

                    auto permutations = math_combinatorics::getPermutations(numbers, 3);

                    for (auto p : permutations)
                    {
                        auto delta_1 = p[1] - p[0];
                        auto delta_2 = p[2] - p[1];

                        if (delta_1 == delta_2)
                        {
                            //cout << "prime: " << *prime << ", valid permutations: " << numbers.size() << ", delta: " << delta_1 << endl;
                            //cout << p[0] << ", " << p[1] << ", " << p[2] << endl;

                            stringstream ss;
                            ss << p[0] << p[1] << p[2];
                            concatenations.insert(ss.str());
                        }
                    }
                }
            }
        }

        for (auto r : concatenations)
        {
            cout << r << endl;
        }

        long elapsedTimeMs = timer.getElapsed();
        cout << "took [ms]: " << elapsedTimeMs << endl;   
    }
}