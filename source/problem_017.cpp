#include <iostream>
#include <map>
#include <cassert>

using namespace std;

namespace project_euler
{

    static map<int, int> singleNumberMap = {
        { 1, 3 } // one
        , { 2, 3 } // two
        , { 3, 5 } // three
        , { 4, 4 } // four
        , { 5, 4 } // five
        , { 6, 3 } // six
        , { 7, 5 } // seven
        , { 8, 5 } // eight
        , { 9, 4 } // nine
    };
    
    const int overHundred = 7; // "hundred"
    const int overHundredNotEven = 3; // "and"

    static map<int, int> tenToTwentyNumberMap = {
        { 10, 3 } // ten
        , { 11, 6} // eleven
        , { 12, 6} // twelve
        , { 13, 8} // thirteen
        , { 14, 8} // fourteen
        , { 15, 7} // fifteen
        , { 16, 7} // sixteen
        , { 17, 9} // seventeen
        , { 18, 8} // eighteen
        , { 19, 8} // nineteen
    };

    static map<int, int> tensNumberMap = {
        { 2, 6} // twenty
        , { 3, 6} // thirty
        , { 4, 5} // forty
        , { 5, 5} // fifty
        , { 6, 5} // sixty
        , { 7, 7} // seventy
        , { 8, 6} // eighty
        , { 9, 6} // ninety
    };

    int getNumberInEnglishRep(int number)
    {
        assert(number > 0 && number < 1000);

        int result = 0;
        int h = number / 100;
        if (h > 0)
        {
            result += singleNumberMap[h];
            result += overHundred;
        }
        int hr = number % 100;
        if (h > 0 && hr != 0)
        {
            result += overHundredNotEven;
        }
        if (hr != 0)
        {
            int d = hr / 10;
            if (d > 0)
            {
                // special case of 10-19
                if (d == 1)
                {
                    result += tenToTwentyNumberMap[hr];
                    return result;
                }
                else
                {
                    result += tensNumberMap[d];
                }
            }

            int dr = hr % 10;
            if (dr > 0)
            {
                result += singleNumberMap[dr];
            }
        }
        return result;
    }
    //! If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
    //! If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
    // -> OK (21124)
    void problem_017()
    {
        cout << getNumberInEnglishRep(342) << endl;

        const int limit = 999;

        long result = 0;
        for (int i = 1; i <= limit; i++)
        {
            result += getNumberInEnglishRep(i);
        }
        result += 11; // "one thousand" == 11
        cout << result << endl;
    }
}