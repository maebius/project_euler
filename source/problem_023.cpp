#include <iostream>
#include <vector>
#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
    /*
    A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

*/
    //! -> OK (4179871)
    void problem_023()
    {
        const int start = 1;
        const int last = 28123;

        vector<int> abundantNumbers;

        math_natural_helpers::getAbundantNumbers(start, last, abundantNumbers);

        //for (auto n : abundantNumbers) cout << n << endl;
        //cout << "count: " << abundantNumbers.size() << " (1st: " << abundantNumbers[0] << ")" << endl;

        vector<bool> check;
        check.resize(last + 1, false);

        for (int i = 0; i < abundantNumbers.size(); i++)
        {
            int first = abundantNumbers[i];
            for (int j = i; j < abundantNumbers.size(); j++)
            {
                int second = abundantNumbers[j];
                int sum = first + second;
                if (sum > last)
                {
                    break;
                }
                check[sum] = true;
            }   
        }

        int resultCount = 0;
        long resultSum = 0;
        for (int i = 1; i < check.size(); i++)
        {
            if (!check[i])
            {
                //cout << i << endl;
                resultCount++;
                resultSum += i;
            }
        }

        cout << "count: " << resultCount << ", sum: " << resultSum << endl;
    }
}