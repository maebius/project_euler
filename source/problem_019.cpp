#include <iostream>

using namespace std;

namespace project_euler
{

    bool isLeapYear(int year)
    {
        bool divByFour = year % 4 == 0;
        bool century = year % 100 == 0;
        
        if (divByFour && !century)
        {
            return true;
        }
        if (!divByFour)
        {
            return false;
        }
        return year % 400 == 0;
    }

    bool addWeek(const bool isLeapYear, int& year, int& month, int& day)
    {
        day += 7;
        if ((month == 9 || month == 4 || month == 6 || month == 11)
            && day > 30)
        {
            month++;
            day -= 30;
        }
        else if (month == 2)
        {
            if (isLeapYear && day > 29)
            {
                month++;
                day -= 29;
            }
            else if (!isLeapYear && day > 28)
            {
                month++;
                day -= 28;
            }
        }
        else if (day > 31)
        {
            month++;
            day -= 31;

            if (month == 13)
            {
                month = 1;
                year++;

                return true;
            }
        }
        return false;
    }

    /*
You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
    */
    //! -> OK (171)
    void problem_019()
    {
        int day = 7;
        int month = 1;
        int year = 1900;

        int sundayOnFirstCount = 0;

        bool leapYear = isLeapYear(year);

        while (year < 2001)
        {
            if (year > 1900 && day == 1)
            {
                sundayOnFirstCount++;
            }
            if (addWeek(leapYear, year, month, day))
            {
                leapYear = isLeapYear(year);
            }
        }

        cout << "number of sundays on 1st: " << sundayOnFirstCount << endl;
    }
}