#include <iostream>

#include "mathPrimeHelpers.h"
#include "SimpleTimer.h"

using namespace std;

namespace project_euler
{
    //! By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
    //! What is the 10 001st prime number?
    // -> OK (answer 104743)
    void problem_007()
    {
        const long targetN = 10001;
        //const long targetN = 1000;
       
        time_helpers::SimpleTimer<> timer;

        long answer = math_prime_helpers::getNthPrimeBruteForce(targetN);
        //long answer = math_prime_helpers::getNthPrimeApproximationSieve(targetN);

        long elapsedTimeMs = timer.getElapsed();

        cout << targetN << "th prime is " << answer << endl;
        cout << "took [ms]: " << elapsedTimeMs << endl;
    }
}