#include <iostream>
#include <cmath>
#include <vector>
#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
/*
Take the number 192 and multiply it by each of 1, 2, and 3:

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 
918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as 
the concatenated product of an integer with (1,2, ... , n) where n > 1?
*/

    //! -> OK (932718654)
    bool containsDuplicates(std::vector<short> digits)
    {
        std::sort(digits.begin(), digits.end());
        auto iterator = std::adjacent_find(digits.begin(), digits.end());
 
        return iterator != digits.end();
    }

    long long getLargestPandigital(long long number, long long limit)
    {
        vector<short> digits;
        math_natural_helpers::getDigitsForBase(number, 10, digits);
        
        if (containsDuplicates(digits))
        {
            return -1;
        }
           
        int multiplier = 1;
        long long current = number;
        long long prev = current;
        
        do
        {
            math_natural_helpers::getDigitsForBase(prev, 10, digits);
            if (digits.size() == 9 && !containsDuplicates(digits) && std::find(digits.begin(), digits.end(), 0) == digits.end())
            {
                prev = current;
            }
            multiplier++;

            long long add = multiplier * number;
            digits.clear();
            math_natural_helpers::getDigitsForBase(add, 10, digits);
            long shift = pow(10, digits.size());

            current *= shift;
            current += add;
        } 
        while (current < limit);

        cout << "getLargest - number: " << number << " : " << prev << " (mult: " << (multiplier - 1) << ")" << endl;

        return prev;
    }

    void problem_038()
    {

        long long limit = 1000000000;
        long long start = 9;
        long long tooBig = 10000;

        long long largest = 0;

        long long current = start;
        long long chosen = current;
        while (current < tooBig)
        {
            auto res = getLargestPandigital(current, limit);
            if (res > largest)
            {
                largest = res;
                chosen = current;
            }
            //current *= 10;
            //current += 9;
            current++;
        }

        vector<short> digits;
        math_natural_helpers::getDigitsForBase(largest, 10, digits);
     
        cout << "[" << chosen << "]: " << largest << " [digits: " << digits.size() << "]" << endl;
    }
}