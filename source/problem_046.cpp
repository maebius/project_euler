#include <iostream>
#include <vector>
#include "mathPrimeHelpers.h"
#include "MathHelpers.h"

using namespace std;

namespace project_euler
{
/*
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×1^2
15 = 7 + 2×2^2
21 = 3 + 2×3^2
25 = 7 + 2×3^2
27 = 19 + 2×2^2
33 = 31 + 2×1^2

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
*/
    //! -> OK (5777)
    void problem_046()
    {
        const long start = 9;
        //const long start = 5775;
        const long limit = 1000000;
        //const long limit = 100;
        vector<bool> primes;

        math_prime_helpers::getPrimesBelowEratosthenesSieveVector(limit, primes);

        for (long i = start; i < limit; i+=2)
        {
            if (primes[i])
            {
                continue;
            }
            
            bool found = false;
            long long j = -1;
            long long result = 1;

            while (!found && j < i - 2)
            {
                j += 2;
                if (primes[j])
                {
                    long candidate = i - j;

                    //cout << "candidate: " << candidate << endl;

                    if (candidate % 2 != 0)
                    {
                        continue;
                    }

                    found = MathHelpers::isSqrtInteger(candidate / 2, result);
                }
            }
            if (found)
            {
                cout << i << ":\t" << j << "\t+\t2 * " << result << "^2" << endl;
            }
            else
            {
                cout << "result: " << i << endl;
                break;
            }
        }
    }
}