#include <iostream>
#include <vector>

#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
/*
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
*/
    //! -> OK (40730)
    static std::vector<unsigned long> g_factorialCache { 1 };
    unsigned long getFactorial(unsigned int n)
    {    
        if (n == 0)
        {
            return 1;
        }

        int s = g_factorialCache.size();
        if (n < s)
        {
            return g_factorialCache[n-1];
        }

        long result = g_factorialCache[s - 1];

        while (s < n)
        {
            s++;
            result *= s;
            g_factorialCache.push_back(result);
        }

        return result;
    }

    void problem_034()
    {
        // TODO: how to determine a reasonable upper limit?
        const long upperLimit = 100000;
        
        vector<long> result;
        long resultSum = 0;

        for (long i = 3; i < upperLimit; i++)
        {
            vector<short> digits;
            math_natural_helpers::getDigitsForBase(i, 10, digits);

            long sum = 0;               
            for (int j = 0; j < digits.size(); j++)
            {
                //cout << "digit: " << digits[j] << endl;

                sum += getFactorial(digits[j]);
            }

            if (i == sum)
            {
                cout << i << endl;
                resultSum += i;
                result.push_back(i);
            } 
        }
        cout << "[" << result.size() << "] -> " << resultSum << endl;
    }
}