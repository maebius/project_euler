#include <iostream>
#include <vector>
#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
/*
An irrational decimal fraction is created by concatenating the positive integers:

0.123456789101112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If d_n represents the nth digit of the fractional part, find the value of the following expression.

d_1 × d_10 × d_100 × d_1000 × d_10000 × d_100000 × d_1000000
*/
    //! -> OK (210)
    short getNthDigit(long n)
    {
        if (n == 1)
        {
            return 1;
        }

        // we use this as a zero-based index
        long i = n - 1;
        //long i = n;

        long long biggestIndex = 1;
        long long valueLimit = 1;
        long long digits = 0;

        long long startIndex = 1;
        long long startValue = 1;

        while (biggestIndex < i)
        {
            startIndex = biggestIndex;
            startValue = valueLimit;
            digits++;

            long newValueLimit = valueLimit * 10;  
            long addToIndex = digits * (newValueLimit - valueLimit);
            
            valueLimit = newValueLimit;
            
            //cout << endl << "start index: " << startIndex << ", add:" << addToIndex << ", digits: " << digits << ", value start/limit: " << startValue << "/" << valueLimit << endl;
            //for (int t = startValue, v = startIndex; t < valueLimit; t++, v += digits) cout << "[" << v << "] " <<  t << endl;
    
            biggestIndex += addToIndex;
        }
        
        //cout << "\tdigits: " << digits << ", start index/value: " << startIndex << "/" << startValue << ", biggest index/value: " << (biggestIndex-digits) << "/" << (valueLimit-1) << endl; 

        long long delta = n - startIndex;
        long long value = startValue + delta / digits;
        long long subIndex = delta % digits;

        //cout << "\tn:" << n << ", delta: " << delta << ", digits: " << digits << ", from value: " << value << ", subIndex: " << subIndex << endl;

        vector<short> d;
        math_natural_helpers::getDigitsForBase(value, 10, d);

   
        return d[d.size() - subIndex - 1];
    }


    void problem_040()
    {
        /*
        long test = 3000;
        auto testResult = getNthDigit(test);
        cout << "test: d_" << test << " = " << testResult << endl;
        /*/

        long long resultProduct = 1;
        for (int i = 1; i <= 1000000; i *= 10)
        {
            long digit = getNthDigit(i);

            cout << "d_" << i << " = " << digit << endl;

            resultProduct *= digit;
        }
        cout << resultProduct << endl;
        //*/
    }
}