#include <iostream>
#include <cmath>
#include <map>
#include <vector>
#include <numeric>

using namespace std;

namespace project_euler
{
/*
    Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

1634 = 1^4 + 6^4 + 3^4 + 4^4
8208 = 8^4 + 2^4 + 0^4 + 8^4
9474 = 9^4 + 4^4 + 7^4 + 4^4
As 1 = 1^4 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
*/
    //! -> OK (443839)
    void problem_030()
    {
        const int power = 5;
        const long start = 2;
        // TODO: how to really determine reasonable upper limit? this has no real logic ... just a guess
        const long last = 999999;

        map<int, int> powers;
        for (int i = 0; i < 10; i++)
        {
            powers[i] = pow(i, power);
        }

        vector<long> result;

        for (long i = start; i <= last; i++)
        {
            //cout << "i=" << i << endl;

            long sum = 0;
            long current = i;            
            do
            {
                sum += powers[current % 10];
                current /= 10;
            } 
            while (current != 0);

            //cout << endl << "sum: " << sum << endl;

            if (i == sum)
            {
                result.push_back(i);
            }
        }
        long totalSum = accumulate(result.begin(), result.end(), 0);

        cout << "items [" << result.size() << "]: ";
        for_each(result.begin(), result.end(), [](const long v) { cout << v << ", "; });
        cout << endl;
        cout << "sum: " << totalSum << endl;
    }
}