#include <iostream>
#include <vector>
#include <cmath>
#include "mathPrimeHelpers.h"

using namespace std;

namespace project_euler
{
/*
The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
*/
    //! -> OK (748317)
    bool isTruncatablePrime(long prime, const std::vector<bool>& primes)
    {
        long right = prime / 10;
        std::vector<short> digits;
        digits.push_back(prime % 10);
        while (right > 0)
        {
            if (!primes[right])
            {
                return false;
            }
            digits.push_back(right % 10);
            right /= 10;
        }
        long left = prime;
        long dec = pow(10, digits.size()-1);
        for (int i = digits.size()-1; i != 0; i--)
        {
            left -= digits[i] * dec;
            if (!primes[left])
            {
                return false;
            }
            dec /= 10;
        }

        return true;
    }

    void problem_037()
    {
        const long start = 11;
        const long limit = 1000000;
        
        vector<bool> primes;
        math_prime_helpers::getPrimesBelowEratosthenesSieveVector(limit, primes);   

        long resultSum = 0;
        vector<long> result;
        for (long i = start; i < limit; i++)
        {
            if (primes[i] && isTruncatablePrime(i, primes))
            {
                result.push_back(i);
                resultSum += i;

                cout << "[" << result.size() << "]: " << i << endl;
            }
        }
        cout << "count: " << result.size() << ", sum: " << resultSum << endl;
    }
}