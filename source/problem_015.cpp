#include <iostream>
#include <utility>
#include <vector>
#include <map>
#include "SimpleTimer.h"
#include "mathCombinatorics.h"

using namespace std;

namespace project_euler
{
    using Node = pair<int, int>;

    void expand(const Node& node, vector<Node>& openList, const int max)
    {
        if (node.first < max)
        {
            openList.push_back(Node(node.first + 1, node.second));
        }
        if (node.second < max)
        {
            openList.push_back(Node(node.first, node.second + 1));
        }
    }

    long numOfPaths(const Node& node, const Node& goal, map<Node, long>& pathCounts, const int gridSize)
    {
        if (node == goal)
        {
            return 1;
        }
        if (pathCounts.find(node) == pathCounts.end())
        {
            vector<Node> children;
            expand(node, children, gridSize);
            long sum = 0;
            for (auto c : children)
            {
                sum += numOfPaths(c, goal, pathCounts, gridSize);
            }
            pathCounts[node] = sum;
         }
        return pathCounts[node]; 
    }

    // -> OK (137846528820)
    void problem_015()
    {
        const int gridSize = 20;
        
        time_helpers::SimpleTimer<> timer;

        // combinatorial;
        //long routes = math_combinatorics::getBinomialCoefficient(gridSize, 2*gridSize);
		using namespace boost::multiprecision;
        long long routes = (long long) math_combinatorics::getBinomialCoefficientT<cpp_int>(gridSize, 2*gridSize);

        // recursive:
        /*
        Node start(0, 0);
        Node goal(gridSize, gridSize);

        map<Node, long> pathCounts;
        long long int routes = numOfPaths(start, goal, pathCounts, gridSize);
        */

        long elapsedTimeMs = timer.getElapsed();
        cout << "routes: " << routes << endl;
        cout << "took [s]: " << (elapsedTimeMs * 0.001f) << endl;
        
        // exponential brute force:
        /*
        timer.restart();      
        vector<Node> openList;
        openList.push_back(start);

        routes = 0;
        
        while (openList.size() > 0)
        {
            auto last = openList.back(); 
            openList.pop_back();
            if (last == goal)
            {
                routes++;
            }
            else 
            {
                expand(last, openList, gridSize);
            }
        }
        
        elapsedTimeMs = timer.getElapsed();

        cout << "routes: " << routes << endl;
        cout << "took [s]: " << (elapsedTimeMs * 0.001f) << endl;
        */
    }
}