#include <iostream>
#include <utility>
#include <cmath>
#include <numeric>
#include <map>

#include "mathNaturalHelpers.h"
#include "mathPrimeHelpers.h"

using namespace std;

namespace project_euler
{

    /*
    Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
*/

    //! -> OK (31626)
    void problem_021()
    {
        const int start = 2;
        const int last = 9999;

        set<long long> primes;
        math_prime_helpers::getPrimesBelowEratosthenesSieve(2 * last, primes);

        map<int, bool> checked;
        set<int> amicables;
        
        for (int i = start; i <= last; i++)
        {
            //cout << "i: " << i << ", sum: " << resultSum << endl;

            if (!checked[i])
            { 
                long sum = math_natural_helpers::getSumOfProperDivisors(i, primes);
                long sumOfSum = math_natural_helpers::getSumOfProperDivisors(sum, primes);

                //cout << "number: " << i << ", sum: " << sum << ", sumOfSum: " << sumOfSum << endl;

                if (i == sumOfSum && sum != sumOfSum)
                {
                    cout << "number: " << i << ", sum: " << sum << ", sumOfSum: " << sumOfSum << endl;
                    
                    checked[i] = true;
                    amicables.insert(i);
                    
                    if (sum <= last)
                    {
                        checked[sum] = true;
                        amicables.insert(sum);
                    }
                }
            }
        }

        long resultSum = accumulate(amicables.begin(), amicables.end(), (long) 0);

        cout << "count: " << amicables.size() << ", sum: " << resultSum << endl;
    }
}