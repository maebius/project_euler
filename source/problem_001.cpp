
#include <vector>
#include <set>
#include <iostream>
#include <numeric>
#include <cmath>

using namespace std;

namespace project_euler
{

    void getUniqueMultiplesOnRange(
        const int first
        , const int last
        , const vector<int>& factors
        , set<int>& outResult)
    {
       for (auto f : factors)
       {
            int current = f;
            // need to start from the first evenly divisable number that is at least "first"
            if (current < first)
            {
                current *= ceil(first / current);
            }

            while (current <= last)
            {
                outResult.insert(current);
                current += f;
            }
        }
    }

    //! If we list all the natural numbers below 10 that are multiples of 3 or 5, 
    //! we get 3, 5, 6 and 9. The sum of these multiples is 23.
    //! Find the sum of all the multiples of 3 or 5 below 1000.
    void problem_001()
    {
        const int first = 0;
        const int last = 999;
        vector<int> factors { 3, 5};
        set<int> result;
        getUniqueMultiplesOnRange(first, last, factors, result);

        int sum = accumulate(result.begin(), result.end(), 0);

        cout << "Sum of all multiples of natural numbers [ ";
        for (auto n : factors) { cout << n << " "; }
        cout << "] between " << first << " and " << last << " is " << sum << " with elements:" << endl;
        for (auto n : result) { cout << n << " "; }
        cout << endl;
    }
}