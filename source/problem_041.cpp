#include <iostream>
#include <vector>
#include "mathCombinatorics.h"
#include "mathPrimeHelpers.h"

using namespace std;

namespace project_euler
{
/*
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. 
For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
*/
    //! -> OK (7652413)
    long getBiggestPrimeFromPermuation(const vector<short> digits, vector<bool> primes)
    {
        auto combinations = math_combinatorics::getPandigitals<short>(digits);
        
        vector<long> panDigitals;
        for_each(combinations.begin()
            , combinations.end()
            , [&panDigitals](const vector<short>& v) 
                {  
                    long number = 0;
                    long dec = 1;
                    for (int i = 0; i < v.size(); i++)
                    {
                        number += (dec * v[i]);
                        dec *= 10;
                    }
                    panDigitals.push_back(number);
                });
        sort(panDigitals.begin(), panDigitals.end(), std::greater<long>());
        for (auto p : panDigitals)
        {
            if (primes[p])
            {
                return p;
            }
        }
        return -1;
    }

    const vector<short> g_digits { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  
    void problem_041()
    {
        const long limit = 987654321;
        vector<bool> primes;
        math_prime_helpers::getPrimesBelowEratosthenesSieveVector(limit + 1, primes);

        vector<short> elements { g_digits };
        long biggestPrime = -1;
        while (biggestPrime == -1 && elements.size() > 0)
        {
            cout << "get biggest for size: " << elements.size() << endl;

            biggestPrime = getBiggestPrimeFromPermuation(elements, primes);
            elements.pop_back();
        }

        cout << biggestPrime << endl;
    }
}