#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

namespace project_euler
{

    //! Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
    // -> OK (5537376230)
    void problem_013()
    {
        vector<string> lines;

        fstream fin("problem_013_number.txt", fstream::in);
        string line;
        while (getline(fin, line))
        {
            lines.push_back(line);  
        }     

        //for (auto l : lines) cout << l << endl; 

        if (lines.size() == 0)
        {
            return;
        } 

        vector<int> resultDigits;

        const int digits = lines[0].size();
        int overflow = 0;
        for (int i = digits-1; i >= 0; i--)
        {
            int current = overflow;
            for (auto l : lines)
            {
                int d = l[i] - '0';
                current += d;
            }

            resultDigits.push_back( current % 10 );
            overflow = current / 10;
        }
        while (overflow > 9)
        {
            resultDigits.push_back( overflow % 10 );
            overflow /= 10;
        }
        if (overflow > 0)
        {
            resultDigits.push_back(overflow);
        }

        reverse(resultDigits.begin(), resultDigits.end());

        int resultSize = resultDigits.size();
        cout << "result length: " << resultSize << endl;
        cout << "10 first digits: ";
        for (int i = 0; i < 10; i++)
        {
            cout << resultDigits[i];
        } 
        cout << endl;
    }
}