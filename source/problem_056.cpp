#include <iostream>
#include <cmath>
#include "SimpleTimer.h"
#include "mathNaturalHelpers.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

using namespace std;
using namespace math_natural_helpers;

namespace project_euler
{
/*
A googol (10^100) is a massive number: one followed by one-hundred zeros; 
100^100 is almost unimaginably large: one followed by two-hundred zeros. 
Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, a^b, where a, b < 100, what is the 
maximum digital sum?

*/

	// -> OK (972)

	int bruteForce(int aMin, int aMax, int bMin, int bMax)
	{
		int biggest = 0;

		for (int b = bMin; b <= bMax; b++)
		{
			for (int a = aMin; a <= aMax; a++)
			{
				vector<short> digits;
				power(a, b, digits);
				int result = 0;
				for_each(digits.begin(), digits.end()
					, [&result](const short d)
					{
						result += d;
					});

				biggest = max(biggest, result);
			}
		}

		return biggest;
	}

	void problem_056()
	{
		time_helpers::SimpleTimer<> timer;

		const int aMin = 1;
		const int aMax = 99;
		const int bMin = 1;
		const int bMax = 99;

		int largest = bruteForce(aMin, aMax, bMin, bMax);

		cout << "largest: " << largest << endl;

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}