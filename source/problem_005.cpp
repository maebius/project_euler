#include <iostream>

using namespace std;

namespace project_euler
{
    //! 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
    //! What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
    void problem_005()
    {
        // brute force ... must be way fancier solution
        long biggestDiviser = 20;
        long candinate = 0; 

        long i;
        do
        {
            candinate += biggestDiviser;
            i = biggestDiviser;
            while (i > 0)
            {
                if (candinate % i != 0)
                {
                    break;
                }
                i--;
            }
        } while (i > 0);
    
        cout << "Smalles number that is evenly divisible for all numbers [1, " << biggestDiviser << "] is " << candinate << endl;
    }
}