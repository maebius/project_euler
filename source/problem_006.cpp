#include <iostream>

using namespace std;

namespace project_euler
{
    long sumOfConsecutiveNumbers(long first, long last)
    {
        return (long) ((last + first) * (last - first + 1) * 0.5f);
    }

    long sumOfConsecutiveSquares(long last)
    {
        // source: http://mathforum.org/library/drmath/view/56982.html
        // 
        //  n
        // Sum [i^2]
        // i=1

        return last * (last + 1) * (2*last + 1) / 6; 
    }

    //! The sum of the squares of the first ten natural numbers is, 1^2 + 2^2 + ... + 10^2 = 385
    //! The square of the sum of the first ten natural numbers is, (1 + 2 + ... + 10)^2 = 552 = 3025
    //! Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
    //! Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
    //! -> OK (answer 25164150)
    void problem_006()
    {
       const long last = 100;

       const long sum = sumOfConsecutiveNumbers(1, last);
       const long squareOfSum = sum * sum;
       const long sumOfSquares = sumOfConsecutiveSquares(last);
       const long result = squareOfSum - sumOfSquares;

       cout << "Difference between sum of the suares and squre of the sum for " << last << " natural numbers: " << result << endl;
    }
}