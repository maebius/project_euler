#include <iostream>
#include <utility>
#include <vector>
#include "mathPrimeHelpers.h"
#include "mathNaturalHelpers.h"

using namespace std;

namespace project_euler
{
/*
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
*/

    //! -> OK (55)
    bool isCircular(long long prime, std::vector<bool>& isPrimes, std::vector<short>& cache)
    {
        std::vector<long long> circulars;
        std::vector<short> digits;
        math_natural_helpers::getDigitsForBase(prime, 10, digits);
        unsigned int s = digits.size();

        //cout << "isCircular - prime: " << prime << ", digits: " << s << endl;

        for (int i = 0; i < s; i++)
        {
            long long c = digits[i];
            int next = (i + 1) % s;
            long dec = 10;
            //cout << "c: " << c << ", i: " << i << ", next: " << next << endl;

            while (next != i)
            {
                c += dec * digits[next];
                dec *= 10;
                next = (next + 1) % s;
            }
            circulars.push_back(c);
            //cout << c << endl;
        }

        //cout << "circulars [" << circulars.size() << "]: ";
        //std::for_each(circulars.begin(), circulars.end(), [](const long long c) { cout << c << " "; }); cout << endl; 

        std::vector<size_t> indices;
        for (auto c : circulars)
        {
            //cout << "c: " << c << ", isPrimes.size() = " << isPrimes.size() << endl;

            if (isPrimes[c])
            {
                indices.push_back(c);   
                //cout << "found: " << c << endl;
            }
            
        }
        bool circular = circulars.size() == indices.size();
        short val = circular ? 1 : -1;
        for (auto c : circulars)
        {
            cache[c] = val;
        }
        return circular;
    }

    void problem_035()
    {
        const long start = 0;
        const long limit = 1000000;
      
        std::vector<bool> isPrimes;
        math_prime_helpers::getPrimesBelowEratosthenesSieveVector(limit, isPrimes);

        vector<short> cache ( isPrimes.size(), 0 );

        int resultCount = 0;
        
        for (long i = start; i < limit; i++)
        {
            if (isPrimes[i] && (cache[i] == 1 || isCircular(i, isPrimes, cache)))
            {
                resultCount++;
                cout << i << endl;
            }
        }
        cout << "[" << resultCount << "]" << endl;
    }
}