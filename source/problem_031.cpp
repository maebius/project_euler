#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

/*
In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?
*/
//! -> OK (73682)
namespace project_euler
{
    const int g_target = 200;
    const std::vector<int> g_coinValues
    {
        200, 100, 50, 20, 10, 5, 2, 1
    };
 
    struct coinRecord
    {
        int     value;
        int     count;
    };

    std::vector<coinRecord>         g_records;
    std::vector<std::vector<int>>   g_combinations;
 
    void expand(int toGo, int index)
    {
        if (index == g_records.size())
        {
            return;
        }

        coinRecord& record = g_records[index];

        int coinCount = toGo / record.value;

        while (coinCount >= 0)
        {
            record.count = coinCount;
            int nextToGo = toGo - record.count * record.value;

            if (nextToGo == 0)
            {
                std::vector<int> selected;
                for (auto r : g_records)
                {
                    selected.insert(selected.end(), r.count, r.value);
                }
                g_combinations.push_back(selected);
            }
            else if (nextToGo > 0)
            {
                expand(nextToGo, index + 1); 
            }

            // reset for next round
            for (auto r : g_records)
            {
                r.count = 0;
            }
            coinCount--;
        }
    }

    void problem_031()
    {   
        for (auto v : g_coinValues)
        {
            g_records.push_back( coinRecord { v, 0 } );
        }
        expand(g_target, 0);

        cout << "result: " << g_combinations.size() << endl;
        
        /*int i = 1;
        for (auto c : g_combinations)
        {
            cout << "[" << i << "]: ";
            int sum = 0;
            std::for_each(c.begin(), c.end(), [&sum](const int v) { cout << v << ", "; sum += v;});
            cout << " (= " << sum << ")" << endl;
            i++;
        }*/
    }
}