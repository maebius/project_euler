#include <iostream>
#include "SimpleTimer.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");
#include <string>
#include <algorithm>
#include "MathHelpers.h"
#include <boost/multiprecision/cpp_int.hpp>

using namespace std;
using namespace boost::multiprecision;

namespace project_euler
{
/*
If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.

Not all numbers produce palindromes so quickly. For example,

349 + 943 = 1292,
1292 + 2921 = 4213
4213 + 3124 = 7337

That is, 349 took three iterations to arrive at a palindrome.

Although no one has proved it yet, it is thought that some numbers, like 196, 
never produce a palindrome. A number that never forms a palindrome through the 
reverse and add process is called a Lychrel number. Due to the theoretical 
nature of these numbers, and for the purpose of this problem, we shall assume 
that a number is Lychrel until proven otherwise. In addition you are given that 
for every number below ten-thousand, it will either (i) become a palindrome in 
less than fifty iterations, or, (ii) no one, with all the computing power that 
exists, has managed so far to map it to a palindrome. In fact, 10677 is the
first number to be shown to require over fifty iterations before producing 
a palindrome: 4668731596684224866951378664 (53 iterations, 28-digits).

Surprisingly, there are palindromic numbers that are themselves Lychrel 
numbers; the first example is 4994.

How many Lychrel numbers are there below ten-thousand?

NOTE: Wording was modified slightly on 24 April 2007 to emphasise the 
theoretical nature of Lychrel numbers.

*/
	// -> OK (249)

	// TODO: unify this with MathHelpers::reverse
	template<typename T>
	T reverse(T number)
	{
		//cout << "reverse: " << number << endl;
		std::ostringstream ss;
		ss << number;
		std::string rev{ ss.str() };
		reverse(rev.begin(), rev.end());

		const auto strBegin = rev.find_first_not_of("0");
		rev = rev.substr(strBegin);

		//cout << "reverse result: " << rev << endl;
		return T(rev);
	}

	// TODO: unify this with MathHelpers::isPalindrome
	template<typename T>
	bool palindrome(const T& number)
	{
		T reversed = reverse<T>(number);
		return number == reversed;
	}

	template<typename T>
	bool isReverseAddPalindrome(T number, T& add)
	{
		T rev = reverse(number);

		//cout << "isReverseAddPalindrome - n: " << number << ", rev: " << rev << endl;

		add = number + rev;
		//cout << "n: " << number << ", rev: " << rev << ", res: " << add << endl;
		return palindrome(add);
	}

	template<typename T>
	bool isPalindromicInSequence(T number, int iteration, const int limit)
	{	
		//cout	<< "isPalindromicInSequence - n: " << number 
		//		<< ", ite: " << iteration << ", l: " << limit << endl;

		if (iteration == limit)
		{
			return false;
		}
		T result;
		if (isReverseAddPalindrome<T>(number, result))
		{
			return true;
		}
		return isPalindromicInSequence<T>(result, iteration + 1, limit);
	}

	int getNumberOfLychrelNumbers(int start, int limit, int iterations)
	{
		using Precision = cpp_int;

		int result = 0;
		for (int i = start; i < limit; i++)
		{
			if (!isPalindromicInSequence<Precision>(i, 0, iterations))
			{
				//cout << "lychrel: " << i << endl;
				result++;
			}
			/* else
			{
				cout << "not lychrel: " << i << endl;
			} */
		}
		return result;
	}

	void problem_055()
	{
		time_helpers::SimpleTimer<> timer;

		const int start = 1;
		const int limit = 10000;
		//const int start = 80;
		//const int limit = 81;
		const int iterations = 50;

		int count = getNumberOfLychrelNumbers(start, limit, iterations);

		cout	<< "lychrel numbers [" << start << ", " << limit << "]: " 
				<< count << endl;

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}