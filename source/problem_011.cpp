#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

namespace project_euler
{
    //! In the 20×20 grid below, four numbers along a diagonal line have been marked in red.
    //! The product of these numbers is 26 × 63 × 78 × 14 = 1788696.
    //! What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?
    //! -> OK (70600674)
    void problem_011()
    {
        const unsigned int adjacentCount = 4;

        vector<vector<unsigned int>> grid;

        fstream fin("problem_011_number.txt", fstream::in);
        string line;
        while (getline(fin, line))
        {
            grid.push_back(vector<unsigned int>());
            stringstream iss(line);
            unsigned int number;
            while (iss >> number)
            {
                grid.back().push_back(number);
            }
        }

        long long int biggest = 0;
        for (unsigned int y = 0; y < grid.size() - adjacentCount + 1; y++)
        {
            for (unsigned int x = 0; x < grid[y].size() - adjacentCount + 1; x++)
            {
                // right
                long long int right = 1;
                for (unsigned int i = 0; i < adjacentCount; i++)
                {
                    right *= grid[y][x + i];
                }

                // down
                long long int down = 1;
                for (unsigned int i = 0; i < adjacentCount; i++)
                {
                    down *= grid[y + i][x];
                }

                // diagonal right descending
                long long int diagRight = 1;
                for (unsigned int i = 0; i < adjacentCount; i++)
                {
                    diagRight *= grid[y + i][x + i];
                }
                // diagonal left descending
                long long int diagLeft = 1;
                for (unsigned int i = 0; i < adjacentCount; i++)
                {
                    diagLeft *= grid[y + adjacentCount - i - 1][x + i];
                }

                biggest = max(biggest, right);
                biggest = max(biggest, down);
                biggest = max(biggest, diagRight);
                biggest = max(biggest, diagLeft);
            }
        }

        cout << "Biggest product for " << adjacentCount << " adjacents: " << biggest << endl;

        //for (auto l : grid) { for (auto n : l) { cout << n << " "; }; cout << endl; }
    }
}