#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <limits>
#include "SimpleTimer.h"

#include <boost/algorithm/string/classification.hpp> // Include boost::for is_any_of
#include <boost/algorithm/string/split.hpp> // Include for boost::split

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

using namespace std;

namespace project_euler
{
/*

Each character on a computer is assigned a unique code and the preferred 
standard is ASCII (American Standard Code for Information Interchange). 
For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII, 
then XOR each byte with a given value, taken from a secret key. The advantage 
with the XOR function is that using the same encryption key on the cipher text, ¨
restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text 
message, and the key is made up of random bytes. The user would keep the 
encrypted message and the encryption key in different locations, and without 
both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified 
method is to use a password as a key. If the password is shorter than the 
message, which is likely, the key is repeated cyclically throughout the message.
 The balance for this method is using a sufficiently long password key for 
 security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower 
case characters. Using cipher.txt (right click and 'Save Link/Target As...'), 
a file containing the encrypted ASCII codes, and the knowledge that the plain 
text must contain common English words, decrypt the message and find the sum of 
the ASCII values in the original text.
*/
// OK -> (107359)

	std::vector<int> getMessage(std::string filename)
	{
		ifstream t(filename);
		stringstream buffer;
		buffer << t.rdbuf();
		string s{buffer.str()};
		t.close();

		vector<string> words;
		boost::split(words, s, boost::is_any_of(", "), boost::token_compress_on);

		vector<int> asInt(words.size());
		for (int i = 0; i < words.size(); i++)
		{
			asInt[i] = atoi(words[i].c_str());
		}
		return asInt;
	}

	bool getNextKey(std::vector<int>& key)
	{
		const int lastValue = 127;
		const int firstValue = 0;

		int lastIndex = key.size()-1;
		int currentIndex = lastIndex;
		while (key[currentIndex] == lastValue && currentIndex > 0)
		{
			currentIndex--;
		}
		if (currentIndex == 0 && key[currentIndex] == lastValue)
		{
			return false;
		}
		key[currentIndex] = key[currentIndex] + 1;
		for (int i = currentIndex + 1; i <= lastIndex; i++)
		{
			key[i] = firstValue;
		} 
		return true;
	}

	const int FOUND_RATIO_REQUIREMENT = 2;
	const int NOT_FOUND_LIMIT = 5;
	const int FOUND_MIN_LIMIT = 10;
	const int WORD_MIN_LENGTH = 3;
	const int GARBLE_MIN_LENGTH = 4;
	const int GARBLE_LIMIT = 10;

	bool keyInvalidCheck(int found, int notFound)
	{
		if (notFound < NOT_FOUND_LIMIT)
		{
			return false;
		}
		if (found == 0)
		{
			return true;
		}
		if (notFound > FOUND_RATIO_REQUIREMENT * found)
		{
			return true;
		}
		return false;
	}

	bool decrypt(
		std::vector<int>& message
		, std::vector<int>& key
		, char* result
		, const std::vector<string>& dictionary
		)
	{
		int msgSize = message.size();
		int keySize = key.size();
		int found = 0;
		int notFound = 0;
		
		/* cout << "decrypt msg[" << msgSize << "], key[" << keySize << "]: ";
		for_each(key.begin(), key.end(), [](int i) { cout << i << " "; });
		cout << endl; */

		int wordStart = 0;
		int garbleStart = 0;
		int garbleCount = 0;
		bool wordStarted = false;
		bool garbleStarted = false;
		
		int last = msgSize - 1;

		for (int i = 0; i < msgSize; i++)
		{
			result[i] = message[i] ^ key[i % keySize];

			// require printable character in ASCII
			if (result[i] == 127
				|| (result[i] > 0 && result[i] < 32)
				|| (result[i] == 0 && i < msgSize - 1))
			{
				return false;
			}

			bool isAlpha = isalpha(result[i]);
			bool isAlnum = isalnum(result[i]);

			if (!garbleStarted && !isAlnum)
			{
				garbleStart = i;
				garbleStarted = true;
			}
			else if (isAlnum && garbleStarted)
			{
				int length = i - garbleStart;
				if (length >= GARBLE_MIN_LENGTH)
				{
					/* string g(result + garbleStart, length);
					cout << "garble[" << garbleCount << "] = '" << g << "'" << endl; */

					garbleCount++;
					if (garbleCount >= GARBLE_LIMIT)
					{
						return false;
					}
				}
				garbleStarted = false;
			}

			if (!wordStarted && isAlpha)
			{
				wordStart = i;
				wordStarted = true;
			}
			else if (wordStarted && !isAlpha)
			{
				wordStarted = false;

				int length = i - wordStart;
				if (length < WORD_MIN_LENGTH)
				{
					continue;
				}

				string s(result + wordStart, length);
				std::transform(s.begin(), s.end(), s.begin(), ::tolower);

				bool contains = std::binary_search(
					dictionary.begin()
					, dictionary.end()
					, s);

				found += contains;
				notFound += !contains;

				//cout << "'" << s << "' : " << contains << endl;
				
				if (keyInvalidCheck(found, notFound))
				{
					/* cout	<< "keyInvalidTrue - found / not: " << found
							<< " / " << notFound << endl; */

					return false;
				}
			}
		}
		// DEBUG start
		/* string asString(result);
		const string TEST("faithfulness");
		if (asString.find(TEST) != string::npos)
		{
			cout << "FOUND '" << TEST << "' in:" << endl;
			cout << asString << endl;

			cout << "key: ";
			for_each(key.begin(), key.end(), [](int i) { cout << i << " "; });
			cout << endl;
		
		}
		return notFoundLimit; */
		// DEBUG end

		//cout << "found: " << found << ", not: " << notFound << endl;

		return 	found > FOUND_MIN_LIMIT
				&& found > notFound * FOUND_RATIO_REQUIREMENT;
	}

	void testAscii()
	{
		char buf[12];
		int i = 0;
		buf[i++] = 97;
		buf[i++] = 98;
		buf[i++] = 114;
		buf[i++] = 97;
		buf[i++] = 104;
		buf[i++] = 97;
		buf[i++] = 109;
		buf[i++] = 32;
		buf[i++] = 71;
		buf[i++] = 65;
		buf[i++] = 89;
		buf[i++] = 0;

		string res(buf);
		cout << res << endl;
	}

	void testXor()
	{
		int a = 25;
		int b = 69;

		int iAb = a ^ b;
		char cAb = ((char) a) ^ ((char) b);

		cout << "int: " << (char) iAb << endl;
		cout << "char: " << cAb << endl;
	}

	void problem_059()
	{
		time_helpers::SimpleTimer<> timer;

		//testAscii(); return;
		//testXor(); return;

		const int NO_DICT_MATCHES_LIMIT = 10;

		vector<int> asInt = getMessage("p059_cipher.txt");
		char buffer[asInt.size() + 1];
		buffer[asInt.size()] = 0;

		//for_each(asInt.begin(), asInt.end(), [](int i){ cout << i << endl; });
		
		// http://www.ef.com/english-resources/english-vocabulary/top-3000-words/
		const string mostFilename("most_common_english_words_3000.txt");
		vector<string> dictionary;
		ifstream mostFile(mostFilename);
		string line;
		while (getline(mostFile, line)) // 6 ms
		{
			std::transform(line.begin(), line.end(), line.begin(), ::tolower);
			dictionary.push_back(line);
		}
		mostFile.close();

		//for_each(dictionary.begin(), dictionary.end(), [](string s){ cout << s << endl; });

		vector<int> key = { 0, 0, 0 };
		//vector<int> key = { 103, 111, 100 }; // DEBUG: right key
		//vector<int> key = { 71, 79, 68 }; // DEBUG: FALSE right key?
		//vector<int> key = { 71, 79, 100 }; // DEBUG: FALSE right key?
		
		bool found = false;
		do 
		{
			if (decrypt(asInt, key, buffer, dictionary))
			{
				found = true;
				break;
			}
		} while (getNextKey(key)); // ~ 11 ms
		//} while (false); // DEBUG
 
		cout << "decrypt with dict[" << dictionary.size() << "]:" << endl;
		if (found)
		{
			cout << "key: ";
			for_each(key.begin(), key.end(), [](int i) { cout << i << " "; });
			cout << endl;

			long sum = 0;
			for (int i = 0; i < asInt.size(); i++) 
			{
				sum += buffer[i];
				cout << buffer[i];
			}
			cout << endl;
			cout << "sum: " << sum << endl;
		}
		else
		{
			cout << "no match" << endl;
		}

		/*
		. muodosta kaikki XYZ keyt, yks kerrallaan loopissa [0,127]
		. käännä message sillä
		. splittaa tulos "., :;"
		. muodosta englannin kielen dictionary 100 most common words tms.
		. laske splitatusta että kuinka monta näistä löytyy most commonista
		. se jonka XYZ keyllä löytyy eniten, voittaa
		. ... tai se eka jonka kaikki sanat löytyy
		. -> tämä joutuu käymään todenännäkösesti kaikki keyt läpi, eli 127^3
		*/

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}