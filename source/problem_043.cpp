#include <iostream>
#include <vector>
#include "mathCombinatorics.h"
#include "SimpleTimer.h"

using namespace std;

namespace project_euler
{
/*
The number, 1406357289, is a 0 to 9 pandigital number because it is made up 
of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.

Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

d2d3d4=406 is divisible by 2
d3d4d5=063 is divisible by 3
d4d5d6=635 is divisible by 5
d5d6d7=357 is divisible by 7
d6d7d8=572 is divisible by 11
d7d8d9=728 is divisible by 13
d8d9d10=289 is divisible by 17
Find the sum of all 0 to 9 pandigital numbers with this property.
*/
    //! -> OK (16695334890)

    static vector<short> g_digits { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    static vector<short> g_divisors { 1, 2, 3, 5, 7, 11, 13, 17 };


    bool isValid(std::vector<short> v)
    {
        //std::for_each(v.begin(), v.end(), [](const short s) { cout << s; }); cout << endl;

        size_t s = v.size() - 1;
        if (s < 2)
        {
            return true;
        }
        int n = v[s - 2] * 100 + v[s - 1] * 10 + v[s];
        int d = g_divisors[s - 2];
        bool cont = n % d == 0;

        //cout << "isValid: " << cont << " (s: " << s << ", n: " << n << ", div: " << d << ")" << endl;

        return cont;
    }

    void problem_043()
    {
        time_helpers::SimpleTimer<> timer;  
        
        auto combinations = math_combinatorics::getPandigitalsWithCondition<short>(g_digits, isValid);
        
        cout << "getPandigitals() took: " << timer.getElapsed() << endl;
     
        long long sum = 0;
      
        for (auto v : combinations)
        {
            long number = 0;
            long dec = 1;
            for (int i = v.size() - 1; i > -1; i--)
            {
                number += (dec * v[i]);
                dec *= 10;
            }
            sum += number;
        }

        cout << "[" << combinations.size() << "] : " << sum << endl;
    }
}