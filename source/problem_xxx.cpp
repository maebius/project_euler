#include <iostream>
#include "SimpleTimer.h"

//#define USE_PROFILING
#include "Profiler.h"
//const char* g_profilerName = "profiler";
// PROFILE_BLOCK(g_profilerName, "id");

using namespace std;

namespace project_euler
{
	void problem_xxx()
	{
		time_helpers::SimpleTimer<> timer;

		// ...

		long elapsedTimeMs = timer.getElapsed();
		cout << "took [ms]: " << elapsedTimeMs << endl;   
	}
}