#include <iostream>
#include <fstream>

#include "GraphUtilities.h"


namespace project_euler
{
    using namespace std;
    using namespace graph_utilities;

    /* 
By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:
    */
    // -> OK (1074)
    void problem_018()
    {
        std::fstream fin("problem_018_triangle.txt", std::fstream::in);
        std::string line;
	
		std::vector<int> previousLine;
		std::vector<int> currentLine;

        while (std::getline(fin, line))
        {
			std::istringstream l(line);
			std::string s;

			currentLine.clear();
			
			while(getline(l, s, ' '))
			{
				int v = std::stoi(s);
				currentLine.push_back(v);
			}

			if (previousLine.size() > 0)
			{
                currentLine[0] += previousLine[0];
                currentLine[currentLine.size()-1] += previousLine[previousLine.size()-1];

				for (int i = 1; i < currentLine.size()-1; i++)
				{
                    currentLine[i] += (previousLine[i-1] > previousLine[i]) 
                        ? previousLine[i-1]
                        : previousLine[i];
                }
			}
			previousLine.clear();
			std::copy(currentLine.begin(), currentLine.end(), std::back_inserter(previousLine));
        }
        std::sort(currentLine.begin(), currentLine.end(), std::greater<int>());
        cout << "biggest sum: " << currentLine[0] << endl;
    }
}