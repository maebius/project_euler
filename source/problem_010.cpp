#include <iostream>
#include <limits>
#include <numeric>
#include <set>
#include "mathPrimeHelpers.h"

using namespace std;

namespace project_euler
{
    long getPrimeSumBruteForce(const long limit, long long int& outSum)
    {
        long current = 3;
        outSum = 2;
        long primeCount = 1;

        const long printDelta = 10000;
        long printLimit = printDelta;

        while (current < limit)
        {
            if (current > printLimit)
            {
                cout << current << " / " << limit << endl;
                printLimit += printDelta;
            }

            // with large limit, this is REALLY bad method to call
            // TODO: need better prime calculation -> eventually (~10 mins): 142913828922 (with 148933 primes) 
            if (math_prime_helpers::isPrimeRecursive(current))
            {
                if (numeric_limits<long long int>::max() - outSum < current)
                {
                    cout << "ERROR! Precision not enough. Use big number implementation. Current prime: " << current << endl;
                    return -1;
                }

                outSum += current;
                primeCount++;
            }
            current += 2;
        }
        return primeCount;
    }


    //! The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
    //! Find the sum of all the primes below two million.
    void problem_010()
    {
        const long limit = 2000000;
        long long int sum = 0;

        //long primeCount = getPrimeSumBruteForce(limit, sum);
        set<long long int> result;
        // (< 1 s) 142913828922 (with 148933 primes) 
        math_prime_helpers::getPrimesBelowEratosthenesSieve(limit, result);
        sum = accumulate(result.begin(), result.end(), (long long int) 0);
        
        long primeCount = result.size();

        cout << "Sum of primes [" << primeCount << "] below " << limit << ": " << sum << endl;
    }
}