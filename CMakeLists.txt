cmake_minimum_required (VERSION 2.6)

set(APP_NAME ProjectEuler)
project (${APP_NAME})

file (GLOB SOURCES "source/*.cpp")
file (GLOB UTILITY_SOURCES "../utilities/source/*.cpp")

set(SRC
  ${SOURCES}
  #${UTILITY_SOURCES}
  "../utilities/source/GraphUtilities.cpp"
  "../utilities/source/SimpleTimer.cpp"
  "../utilities/source/RandomHelpers.cpp"
  "../utilities/source/mathCombinatorics.cpp"
  "../utilities/source/MathHelpers.cpp"
  "../utilities/source/Profiler.cpp"
  "../utilities/source/Debug.cpp"
  "../utilities/source/Poker.cpp"
)

include_directories(
    "include"
    "../utilities/include"
    "/usr/local/include"
    "../external"
    "/Users/maebius/Libraries/boost_1_62_0"
)

link_directories(
    "/usr/local/lib"
)

set(CMAKE_C_FLAGS_DEBUG "-g -Wall")
set(CMAKE_CXX_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG})
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wno-deprecated-declarations -Wno-reorder -O2")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")

set(APP_BIN_DIR "${CMAKE_BINARY_DIR}")

add_executable(${APP_NAME} ${SRC})

## target_link_libraries (${APP_NAME} exiv2)

set_target_properties(
    ${APP_NAME} PROPERTIES 
    RUNTIME_OUTPUT_DIRECTORY  "${APP_BIN_DIR}"
)

set(CMAKE_BUILD_TYPE Release)
